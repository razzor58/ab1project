# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2018-02-13 17:28
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('console', '0053_firmware_file_hash'),
    ]

    operations = [
        migrations.AlterField(
            model_name='firmware',
            name='document',
            field=models.FileField(blank=True, null=True, upload_to='firmwares_new'),
        ),
        migrations.AlterField(
            model_name='firmware',
            name='uploaded_at',
            field=models.DateTimeField(default=django.utils.timezone.now),
        ),
    ]
