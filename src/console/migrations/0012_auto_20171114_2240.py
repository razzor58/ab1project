# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-11-14 15:40
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('console', '0011_auto_20171113_2331'),
    ]

    operations = [
        migrations.AddField(
            model_name='mqttmessage',
            name='dup',
            field=models.BooleanField(default=False),
        ),
        migrations.AddField(
            model_name='mqttmessage',
            name='qos',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='mqttmessage',
            name='redelivered',
            field=models.BooleanField(default=False),
        ),
    ]
