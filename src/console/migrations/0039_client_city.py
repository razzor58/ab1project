# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-12-27 15:34
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('console', '0038_city'),
    ]

    operations = [
        migrations.AddField(
            model_name='client',
            name='city',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='console.City'),
        ),
    ]
