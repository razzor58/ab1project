# -*- coding: utf-8 -*-
# Generated by Django 1.11.5 on 2017-12-20 15:55
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('console', '0034_auto_20171218_2023'),
    ]

    operations = [
        migrations.AddField(
            model_name='gateway',
            name='status',
            field=models.CharField(blank=True, default='New', max_length=20, null=True),
        ),
    ]
