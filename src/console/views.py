# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.shortcuts import redirect
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect,JsonResponse,HttpResponse
from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate
from django.contrib.auth import login
from django.contrib.auth import authenticate
from django.core.checks.security import csrf
from console import models
from console.forms import SignUpForm
from . import forms
import logging
from django.utils import timezone
from datetime import timedelta
import datetime
from collections import defaultdict
from django.forms import modelformset_factory
from pytz import all_timezones as all_tz
import os 


log = logging.getLogger('web')

@login_required(login_url='/console/login')
def index(request):
    users = models.Client.objects.all().select_related('gateway')
    s = models.Sensor.objects.filter(name='VOLT')
    minutes_ago_value = timezone.now()-timedelta(minutes=120)
    rows = models.SensorData.objects.filter(receive_date__gte=minutes_ago_value)    
    sensors = set()
    
    for row in rows:
        s= row.sensor
        s.name = s.name.replace('/','')
        sensors.add(s)
    context = dict(users=users,sensors=list(sensors))
    
    return render(request, 'console/index2.html', context)

@login_required(login_url='/console/login')
def gateway_modal(request):
    pk=request.GET.get('pk')
    gws = models.Gateway.objects.filter(client__pk=pk)
    nodes = models.Node.objects.filter(gateway__in=gws)

    if request.GET.get('mode') == 'nodes':
        context_objects = nodes
    elif request.GET.get('mode') == 'sensors':        
        context_objects = models.Sensor.objects.filter(node__in=nodes)
    else:
        context_objects = gws
    log.info(context_objects)
    context = dict(context_objects=context_objects)



    return render(request, 'console/gateway_modal.html', context)



def index_en(request):
    context = dict(gws=models.Gateway.objects.all())
    return render(request, 'console/index2.html', context)


def utilities(request):
    context = {}
    return render(request, 'console/utilities.html', context)


def resources(request):
    try:
        offset_value = models.AppSettings.objects.get(name='status_seen_offset').value
        offset_time = int(offset_value)*60*60  
    except Exception:
        offset_time = 0
      

    gws = models.Gateway.objects.all().select_related('connection')
    for g in gws:        
        if g.client and g.status=='New':
            g.status='Registered'
            g.save()
        if models.Node.objects.filter(eui=g.name +'-out/0').count() > 0:
            node = models.Node.objects.get(eui=g.name +'-out/0')
        else:
            node = models.Node.objects.get(eui=g.name +'-out/255')
            

        n = models.Node.objects.filter(gateway=g)
        g.nodes = n.count()        
        # b = broker.sl
        g.node_uid=node.name
        g.sensors = models.Sensor.objects.filter(node__in=n).count()
        g.node_type = node.node_type
        g.bat_level = node.bat_level
        g.rssi = node.rssi
        g.library = node.library
        g.firmware_min = node.firmware_min        
        g.send_status_ago = str(timezone.now() - node.send_status).split('.')[0]
        if node.status_seen:
            
            try:
                # seen_sec = int(int(node.status_seen)/1000)
                seen_sec = 0 if len(node.status_seen) < 4 else int(node.status_seen[:-3])
                d = datetime.datetime.fromtimestamp(seen_sec + offset_time) - datetime.datetime.utcfromtimestamp(0) 
            except Exception as e:
                d = 'incorrect value:{}'.format(str(seen_sec) + ';'+ str(offset_time))
            g.status_seen_ago = str(d)
            
    
    nds = models.Node.objects.all()
    for n in nds:        
        if n.gateway.client and n.status=='New':
            n.status='Registered'
            n.save()
        if n.gateway.client:
            n.client = n.gateway.client.email
        n.eui_short = n.eui.split('/')[1]
        n.gw = "-".join(n.eui.split('-')[:2])
        n.sensors = models.Sensor.objects.filter(node=n).count()
        n.time = str(timezone.now() - n.send_status).split('.')[0]
        if n.status_seen:
            #seen_sec = int(n.status_seen[:-3])
            try:
                # seen_sec = int(int(node.status_seen)/1000)
                seen_sec = 0 if len(n.status_seen) < 4 else int(n.status_seen[:-3])
                d = datetime.datetime.fromtimestamp(seen_sec+ offset_time) - datetime.datetime.utcfromtimestamp(0) 
            except Exception as e:
                d = 'incorrect value:{}'.format(n.status_seen)
            n.status_seen_ago = str(d)

    sens = models.Sensor.objects.all()
    for s in sens:
        rows = defaultdict(list)
        for i in models.SensorData.objects.filter(sensor=s).order_by('-receive_date'):
            rows[i.variable_type].append((i.value,i.receive_date))
        s.sens_data = []
        for k,v in rows.items():    
            t = timezone.localtime(v[0][1]).strftime('%H:%M:%S')
            s.sens_data.append({'variable_type':k, 'value':v[0][0],'time':t })

        n.time = str(timezone.now() - n.send_status).split('.')[0]
        s.eui = "-".join(s.node.eui.split('-')[:2])
        s.eui_s = s.node.eui.split('/')[1]

    context = dict(gateways=gws,nodes=nds, sensors=sens)        
    return render(request, 'console/resources.html', context)

def resources_new(request):
    commands = models.MenuGatewayCommand.objects.all()
    context = dict(commands=commands)
    return render(request, 'console/resources_new.html', context)


def users(request):
    
    users = models.Client.objects.all()
    for u in users:
        u.gateways = u.gateway_set.all().count()
        nodes = models.Node.objects.filter(gateway__in=u.gateway_set.all())
        u.nodes = nodes.count()
        u.sensors = models.Sensor.objects.filter(node__in=nodes).count()

    all_g = models.Gateway.objects.filter(client=None)
    context = dict(users=users,all_gateways=all_g)

    return render(request, 'console/users.html', context)


def status(request):
    context = {}
    return render(request, 'console/status.html', context)


def new_tab(request):
    context = {}
    return render(request, 'console/layout_1.html', context)
   


def form_save(request):
    if request.method == 'POST':
        for key,val in request.POST.items():
            setting_record = models.AppSettings.objects.get(name=key)
            setting_record.value = val
            setting_record.save()

    context = get_settings_context()
    return render(request, 'console/settings.html', context)


def settings(request):
    
    if request.method == 'POST':
        for key,val in request.POST.items():
            setting_record = models.AppSettings.objects.get(name=key)
            setting_record.value = val
            setting_record.save()
            
    context = get_settings_context()
    return render(request, 'console/settings.html', context)

def get_settings_context():  
    conn_form = forms.ConnectionForm()
    settings_form = []
    tz = [t for t in all_tz]
    for i in models.AppSettings.objects.all():      
        settings_form.append(forms.AppSettingsForm(instance=i))

    context = {'conn_form': conn_form,'settings_form':settings_form, 'tz':tz}
    return context

def not_found(request):
    context = {}
    response = render_to_response('console/404.html', context)
    response.status_code = 404
    return response



def internal_error(request):
    context = {}
    response = render_to_response('console/500.html', context)
    response.status_code = 500
    return response


def register(request):
     if request.method == 'POST':
         form = UserCreationForm(request.POST)
         if form.is_valid():
             form.save()
             return HttpResponseRedirect('registration/complete')

     else:
         form = UserCreationForm()
         token = {}
        #  token.update(csrf(request))
         token['form'] = form
     return render_to_response('registration/registration.html', token)

def registration_complete(request):
     return render_to_response('registration/registration_complete.html')


def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('index')
    else:
        form = SignUpForm()
    return render(request, 'registration/registration.html', {'form': form})


def livelog(request):
     return render_to_response('console/log.html')
