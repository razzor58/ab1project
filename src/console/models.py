# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from django.db.models import F
import datetime
import pytz
import logging
import os

from . import choices

log = logging.getLogger('fox')


class Client(models.Model):
    # Meta data
    reg_date = models.DateTimeField(auto_now_add=True, blank=False, null=False)

    # Regular credentials
    name = models.CharField(max_length=100, blank=False, null=False)
    email = models.CharField(max_length=100, blank=False, null=False)
    phones = models.CharField(max_length=100, blank=False, null=False)

    # region / time zone/location
    locale = models.CharField(max_length=200, blank=False, null=False)
    city = models.ForeignKey('City',blank=True,null=True)
    # currect status C/O
    status = models.CharField(max_length=50, blank=False, null=False)    

    def __str__(self):
        return self.name


class ClientMessage(models.Model):
    text = models.CharField(max_length=200, blank=False, null=False)
    channel = models.CharField(max_length=10, blank=False, null=False)


class ClientMessageQueue(models.Model):
    create_date = models.DateTimeField(auto_now_add=True, blank=False, null=False)
    message = models.ForeignKey('ClientMessage', on_delete=models.CASCADE, blank=False, null=False)
    client = models.ForeignKey('Client', on_delete=models.CASCADE, blank=False, null=False)
    status = models.IntegerField(default=0)
    delivery_date = models.DateTimeField(auto_now_add=True, blank=False, null=False)


class Connection(models.Model):
    # заполняется при регистрации нового шлюза на основании ID устройства
    name = models.CharField(max_length=50, blank=False, null=False)

    # задержка при отправки пакетов на устройства  Milliseconds
    delay = models.IntegerField(default=0)

    # наш тип коннектора
    network_type =models.CharField(max_length=50, blank=False, null=False)

    # qos
    qos = models.IntegerField(default=0)

    # автоматически их шаблона на сервере
    broker_host = models.CharField(max_length=50, blank=False, null=False)

    # формируется автоматически на основании ID устройства и добавление на сервере MQTT)
    client_id = models.ForeignKey('Client', blank=True, null=True)

    # формируется автоматически на основании “gateway”+ ID устройства + "-in"
    topic_publish = models.CharField(max_length=50, blank=False, null=False)

    # формируется автоматически на основании “gateway”+ ID устройства + "-out"
    topic_subscribe = models.CharField(max_length=50, blank=False, null=False)

    #  формируется автоматически на основании емейла и добавление на сервере MQTT
    user_login = models.CharField(max_length=50, blank=False, null=False)
    password  = models.CharField(max_length=50, blank=False, null=False)

    def __str__(self):
        return self.name


class Gateway(models.Model):
    reg_date = models.DateTimeField(auto_now_add=True,blank=True,null=True)
    name = models.CharField(max_length=80, blank=True, null=True)
    status_broker = models.CharField(max_length=50, blank=True, null=True)
    status = models.CharField(default='New',max_length=20, blank=True, null=True)
    mobile = models.CharField(max_length=50, blank=True, null=True)
    web	= models.CharField(max_length=50, blank=True, null=True)
    uid_gateway = models.CharField(max_length=50, blank=True, null=True)
    connection = models.ForeignKey('Connection', blank=True, null=True)
    status_seen	= models.CharField(max_length=50, blank=True, null=True)
    seen_message = models.CharField(max_length=50, blank=True, null=True)
    # Nodes/Sensors
    nodes_sensors = models.CharField(max_length=50, blank=True, null=True)
    topology = models.CharField(max_length=50, blank=True, null=True)
    client = models.ForeignKey('Client',blank=True,null=True)
    inclusion_mode = models.IntegerField(default=0, blank=False, null=False)

    def __str__(self):
        return self.name

    def attrs(self):
        for attr, value in self.__dict__.iteritems():
            yield attr, value


class Sensor(models.Model):
    reg_date = models.DateTimeField(auto_now_add=True,blank=True,null=True)
    node = models.ForeignKey('Node',blank=True,null=True)
    child_sensor_id = models.IntegerField(blank=True,null=True)
    name = models.CharField(max_length=50, blank=False, null=False)
    node_type = models.CharField(max_length=50, blank=False, null=False)    
    status_seen = models.CharField(max_length=20, blank=False, null=False)
    first_reg_date = models.DateTimeField(auto_now_add=True,blank=True,null=True)

    def __str__(self):
        return self.name


class SensorActualData(models.Model):
    sensor = models.ForeignKey(Sensor,blank=True,null=True,on_delete=models.CASCADE)   
    receive_date = models.DateTimeField(default=timezone.now)
    variable_type = models.CharField(max_length=30, blank=False, null=False)
    value = models.CharField(max_length=50, blank=True, null=True)
    
    def __str__(self):
        return self.variable_type + ':' + self. value

class SensorData(models.Model):
    sensor = models.ForeignKey('Sensor',blank=True,null=True, )    
    receive_date = models.DateTimeField(default=timezone.now)
    variable_type = models.CharField(max_length=30, blank=False, null=False)
    value = models.CharField(max_length=50, blank=True, null=True)
    
    def __str__(self):
        return self.variable_type + ':' + self. value




class Node(models.Model):
    reg_date = models.DateTimeField(auto_now_add=True)
    status  = models.CharField(default='New',max_length=20, blank=False, null=False)
    public_id = models.IntegerField(blank=True,null=True)
    eui = models.CharField(max_length=50, blank=False, null=False)
    parent = models.CharField(max_length=20, blank=False, null=False)    
    send_status = models.DateTimeField(blank=True, null=True)
    name = models.CharField(max_length=50, blank=False, null=False)
    rssi = models.FloatField(default=0.0)
    bat_level = models.FloatField(default=0.0)
    node_type = models.CharField(max_length=50, blank=False, null=False)
    protocol = models.CharField(max_length=50, blank=False, null=False)
    firmware_min = models.CharField(max_length=50, blank=False, null=False)
    firmware_maj = models.CharField(max_length=50, blank=False, null=False)
    library	 = models.CharField(max_length=50, blank=False, null=False)
    status_seen  = models.CharField(max_length=50, blank=False, null=False)
    sensors	 = models.CharField(max_length=50, blank=False, null=False)
    topology = models.CharField(max_length=50, blank=False, null=False)
    gateway = models.ForeignKey('gateway',blank=True,null=True)

    def __str__(self):
        res = self.eui if len(self.name)==0 else self.name
        return res


class NodeBatteryUsage(models.Model):
    timestamp = models.DateTimeField(default=timezone.now)
    node = models.ForeignKey('node',blank=True,null=True)
    level = models.CharField(max_length=50, blank=True, null=True)


    def __str__(self):
        return str(self.node) + ':' + self.level

class RawMessage(models.Model):
    timestamp = models.DateTimeField(auto_now_add=True)
    ch =  models.CharField(max_length=2000, blank=True, null=True)
    method =  models.CharField(max_length=2000, blank=True, null=True)
    properties =  models.CharField(max_length=2000, blank=True, null=True)
    body =  models.CharField(max_length=200, blank=True, null=True)


    def __str__(self):
        return self.body


class MQTTMessage(models.Model):
    """Model for parsed messages from MQTT broker"""

    timestamp = models.DateTimeField(auto_now_add=True)
    consumer_tag = models.CharField(max_length=200, blank=True, null=True)
    delivery_tag = models.CharField(max_length=200, blank=True, null=True)
    exchange = models.CharField(max_length=200, blank=True, null=True)
    redelivered = models.BooleanField(default=False)
    uri = models.CharField(max_length=200, blank=True, null=True)
    cluster_name = models.CharField(max_length=20, blank=True, null=True)
    routing_key = models.CharField(max_length=50, blank=True, null=True)
    payload = models.CharField(max_length=100, blank=True, null=True)
    gateway = models.CharField(max_length=100, blank=True, null=True)
    direction = models.CharField(max_length=3)
    node_id = models.IntegerField()
    child_sensor_id = models.IntegerField(default=0)
    command = models.IntegerField()
    ack = models.IntegerField()
    type = models.IntegerField()
    qos = models.IntegerField(default=0)
    dup = models.BooleanField(default=False)

    def __str__(self):
        return self.timestamp.strftime("%Y-%m-%d %H:%M:%S") + " " + self.routing_key  + ": " + self.payload


class AppSettings(models.Model):    
    name = models.CharField(max_length=100, blank=True, null=True)
    rus_name = models.CharField(max_length=100, blank=True, null=True)
    value = models.CharField(max_length=100, blank=True, null=True)


class HeartbeatTimer(models.Model):
    object_type = models.CharField(max_length=30, blank=True, null=True)
    last_check_date = models.DateTimeField(blank=True,null=True)    
    


class City(models.Model):    
    name = models.CharField(max_length=100, blank=True, null=True)
    short_name = models.CharField(max_length=10, blank=True, null=True)
    rus_name = models.CharField(max_length=100, blank=True, null=True)
    timezone = models.CharField(max_length=50, blank=True, null=True)
    latitude = models.FloatField(blank=True,null=True)
    longitude = models.FloatField(blank=True,null=True)           
    population = models.IntegerField(default=0,null=True) 
    
    
    def __str__(self):        
        try:
            user_timezone = pytz.timezone(self.timezone)
            fmt = '%z'        
            loc_dt = int(user_timezone.localize(datetime.datetime.now()).strftime(fmt)[1:3])
            offset = user_timezone.localize(datetime.datetime.now()).strftime(fmt)[0]
            res = self.name  + "/ " + offset + str(loc_dt)
        except Exception:
            res = self.name                
        return res



class Firmware(models.Model):
    name = models.CharField(max_length=255, blank=True)
    document = models.FileField(upload_to='firmwares_new',blank=True, null=True)
    file_hash = models.CharField(max_length=455, blank=True,null=True)
    file_size = models.FloatField(blank=True,null=True)
    uploaded_at = models.DateTimeField(default=timezone.now)
    uploaded_by = models.ForeignKey(User,blank=True,null=True)
    is_latest = models.IntegerField(default=1,choices=choices.LATEST_CHOICE)
    firmware_type = models.CharField(max_length=100,blank=True, null=True)
    # node = models.ForeignKey('NodeModels',blank=True,null=True)
    node_model = models.ForeignKey('NodeModels',blank=True,null=True)
    file_type = models.IntegerField(default=1,choices=choices.FIRMWARE_CHOICE)

    def __str__(self):
        return self.name



class FirmwareLog(models.Model):
    timestamp = models.DateTimeField(auto_now_add=True) 
    node = models.ForeignKey('Node',blank=True,null=True)
    method = models.CharField(max_length=50, blank=True,null=True)
    request_meta = models.CharField(max_length=500, blank=True)
    request_data = models.CharField(max_length=500, blank=True)
    responce_data = models.CharField(max_length=500, blank=True)

    def __str__(self):
        return self.timestamp.strftime("%Y-%m-%d %H:%M:%S") + " " + str(self.node)


class NodeModels(models.Model):
    name = models.CharField(max_length=200, blank=True,null=True)

    def __str__(self):
        return self.name


class TestingLog(models.Model):
    timestamp = models.DateTimeField(auto_now_add=True) 
    device_name = models.CharField(max_length=100, blank=True,null=True)
    version = models.CharField(max_length=100, blank=True,null=True)
    test_number = models.CharField(max_length=100, blank=True,null=True)
    sta_mac = models.CharField(max_length=100, blank=True,null=True)
    sketch_md5 = models.CharField(max_length=100, blank=True,null=True)

    def __str__(self):
        return self.timestamp.strftime("%Y-%m-%d %H:%M:%S") + " " + str(self.device_name) + ":" + str(self.test_number)



class DeviceCommand(models.Model):
    name = models.CharField(max_length=100, blank=True,null=True) 
    qos = models.IntegerField(blank=True,null=True)
    ask = models.IntegerField(blank=True,null=True)
    mqtt_type = models.IntegerField(blank=True,null=True)

    def __str__(self):
        return self.name + '('+ str(self.qos) + '/' + str(self.ask) + '/' + str(self.mqtt_type) + ')'


class GatewayCommand(models.Model):
    name = models.CharField(max_length=100, blank=True,null=True) 
    qos = models.IntegerField(blank=True,null=True)
    ask = models.IntegerField(blank=True,null=True)
    mqtt_type = models.IntegerField(blank=True,null=True)
    description = models.CharField(max_length=100, blank=True,null=True) 

    def __str__(self):
        return self.name + '('+ str(self.qos) + '/' + str(self.ask) + '/' + str(self.mqtt_type) + ') - ' + self.description


class NodeCommand(models.Model):
    name = models.CharField(max_length=100, blank=True,null=True) 
    qos = models.IntegerField(blank=True,null=True)
    ask = models.IntegerField(blank=True,null=True)
    mqtt_type = models.IntegerField(blank=True,null=True)
    description = models.CharField(max_length=100, blank=True,null=True) 

    def __str__(self):
        return self.name + '('+ str(self.qos) + '/' + str(self.ask) + '/' + str(self.mqtt_type) + ') - ' + self.description


class SensorCommand(models.Model):
    name = models.CharField(max_length=100, blank=True,null=True) 
    qos = models.IntegerField(blank=True,null=True)
    ask = models.IntegerField(blank=True,null=True)
    mqtt_type = models.IntegerField(blank=True,null=True)
    description = models.CharField(max_length=100, blank=True,null=True) 

    def __str__(self):
        return self.name + '('+ str(self.qos) + '/' + str(self.ask) + '/' + str(self.mqtt_type) + ') - ' + self.description



class MenuGatewayCommand(models.Model):
    name = models.CharField(max_length=100, blank=True,null=True) 
    internal_command = models.CharField(max_length=100, blank=True,null=True) 
    broadcast = models.CharField(max_length=100, blank=True,null=True) 
    c_internal = models.CharField(max_length=100, blank=True,null=True) 
    ask = models.IntegerField(blank=True,null=True)
    i_discover_request = models.IntegerField(blank=True,null=True)
    qos = models.IntegerField(blank=True,null=True)
    # mqtt_type = models.IntegerField(blank=True,null=True)
    retained = models.IntegerField(blank=True,null=True)
    description = models.CharField(max_length=100, blank=True,null=True) 

    def __str__(self):
        return self.name + '('+ self.description + ')'



class InternalCommand(models.Model):
    mqtt_type = models.CharField(max_length=10, blank=True,null=True)
    name = models.CharField(max_length=20, blank=True,null=True)
    api_sensor_data = models.CharField(max_length=10, blank=True,null=True) 
    api_type_sensor  = models.CharField(max_length=10, blank=True,null=True) 
    api_message_type  = models.CharField(max_length=10, blank=True,null=True) 
    api_type_data_stream  = models.CharField(max_length=10, blank=True,null=True) 
    api_type_payload  = models.CharField(max_length=10, blank=True,null=True) 


    def __str__(self):
        return self.name


class ApiMessageTypeCommand(models.Model):
    type = models.CharField(max_length=50, blank=True,null=True)
    value = models.CharField(max_length=3, blank=True,null=True)
    comment = models.CharField(max_length=100, blank=True,null=True)

    def __str__(self):
        return self.type + '<--' + value


class ApiTypePayloadCommand(models.Model):
    type = models.CharField(max_length=50, blank=True,null=True)
    value = models.CharField(max_length=3, blank=True,null=True)
    comment = models.CharField(max_length=100, blank=True,null=True)

    def __str__(self):
        return self.type + '<--' + value


class ApiTypeSensorCommand(models.Model):
    type = models.CharField(max_length=50, blank=True,null=True)
    value = models.CharField(max_length=3, blank=True,null=True)
    comment = models.CharField(max_length=100, blank=True,null=True)

    def __str__(self):
        return self.type + '<--' + value

class ApiTypeDataStreamCommand(models.Model):
    type = models.CharField(max_length=50, blank=True,null=True)
    value = models.CharField(max_length=3, blank=True,null=True)
    comment = models.CharField(max_length=100, blank=True,null=True)

    def __str__(self):
        return self.type + '<--' + value