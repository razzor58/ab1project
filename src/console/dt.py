
from django.http import HttpResponseRedirect,JsonResponse,HttpResponse
from console import models
import json
from django.core import serializers
from django.utils import timezone
from datetime import timedelta
import datetime
from django.forms.models import model_to_dict
from collections import defaultdict

def get_gateway_table(request):
    db_data=[]
    try:
        offset_value = models.AppSettings.objects.get(name='status_seen_offset').value
        offset_time = int(offset_value)*60*60  
    except Exception:
        offset_time = 0
      

    gws = models.Gateway.objects.all().select_related('connection')
    for gate in gws:   

        if gate.client and gate.status=='New':
            gate.status='Registered'
            gate.save()
        if models.Node.objects.filter(eui=gate.name +'-out/0').count() > 0:
            node = models.Node.objects.get(eui=gate.name +'-out/0')
        else:
            node = models.Node.objects.get(eui=gate.name +'-out/255')
            
        n = models.Node.objects.filter(gateway=gate)
        g =  model_to_dict(gate)
        g['nodes'] = n.count()
        g['client_email'] = gate.client.email if gate.client is not None else 'no client' 
        g['broker_host'] = gate.connection.broker_host  if gate.connection is not None else 'no connection'           
        # b = broker.sl
        g['node_uid']=node.name
        g['sensors'] = models.Sensor.objects.filter(node__in=n).count()
        g['node_type'] = node.node_type
        g['bat_level'] = node.bat_level
        g['rssi'] = node.rssi
        g['library'] = node.library
        g['firmware_min'] = node.firmware_min        
        g['send_status_ago'] = str(timezone.now() - node.send_status).split('.')[0]
        if node.status_seen:
            try:
                # seen_sec = int(int(node.status_seen)/1000)
                seen_sec = 0 if len(node.status_seen) < 4 else int(node.status_seen[:-3])
                d = datetime.datetime.fromtimestamp(seen_sec + offset_time) - datetime.datetime.utcfromtimestamp(0) 
            except Exception as e:
                d = 'incorrect value:{}'.format(str(seen_sec) + ';'+ str(offset_time))
            g['status_seen_ago'] = str(d)
        db_data.append(g)
    # data = json.loads(db_data,safe=False)
    draw = request.GET.get('draw')
    response = {
        "draw": draw,
        "result": 'ok',
        "data": db_data,
        "recordsFiltered": 100,
        "recordsTotal": 100
    }
    return JsonResponse(response)

def get_node_table(request):

    try:
        offset_value = models.AppSettings.objects.get(name='status_seen_offset').value
        offset_time = int(offset_value)*60*60  
    except Exception:
        offset_time = 0

    gate_name = request.POST.get('gate_name')
    draw = request.GET.get('draw')

    if gate_name == '':
        nds = models.Node.objects.all()
    else:
        gate = models.Gateway.objects.get(name=gate_name)
        nds = models.Node.objects.filter(gateway=gate)
    total =nds.count()
    db_data = []
    for n in nds:        
        if n.gateway.client and n.status=='New':
            n.status='Registered'
            n.save()
        if n.gateway.client:
            n.client = n.gateway.client.email

        node =  model_to_dict(n)
        node['eui_short']= n.eui.split('/')[1]
        node['gw'] = "-".join(n.eui.split('-')[:2])

        if n.gateway.client:
            node['client_email'] = n.gateway.client.email
        else:
            node['client_email']=''
        if n.status_seen:

            #seen_sec = int(n.status_seen[:-3])
            try:

                node['sensors'] = models.Sensor.objects.filter(node=n).count()
                node['time'] = str(timezone.now() - n.send_status).split('.')[0]
                # seen_sec = int(int(node.status_seen)/1000)
                seen_sec = 0 if len(n.status_seen) < 4 else int(n.status_seen[:-3])
                d = datetime.datetime.fromtimestamp(seen_sec+ offset_time) - datetime.datetime.utcfromtimestamp(0) 
            except Exception as e:
                d = 'incorrect value:{}'.format(n.status_seen)
            node['status_seen_ago'] = str(d)
        db_data.append(node)

    response = {
        "draw": draw,
        "result": 'ok',
        "data": db_data,
        "recordsFiltered": total,
        "recordsTotal": total
    }
    return JsonResponse(response)


def get_sensor_table(request):
    node_name = request.POST.get('node_name')
    gate_name = request.POST.get('gate_name')

    if node_name =='':
        sens = models.Sensor.objects.all()

    elif gate_name is not None and node_name is None:
        gateway = models.Gateway.objects.get(name=gate_name)
        sens = models.Sensor.objects.filter(node__in=models.Node.objects.filter(gateway=gateway))
    else:
        node = models.Node.objects.get(eui=node_name)
        sens = models.Sensor.objects.filter(node=node)

    total = sens.count()
    db_data=[]
    for sensor in sens:
        rows = defaultdict(list)

        s =  model_to_dict(sensor)

        s['sens_data'] = []

        for i in models.SensorActualData.objects.filter(sensor=sensor).order_by('-receive_date'):
            rows[i.variable_type].append((i.value,i.receive_date))

        for k,v in rows.items():    
            t = timezone.localtime(v[0][1]).strftime('%H:%M:%S')
            s['sens_data'].append({'variable_type':k, 'value':v[0][0],'time':t })

        # n.time = str(timezone.now() - n.send_status).split('.')[0]
        s['eui'] = "-".join(sensor.node.eui.split('-')[:2])
        s['eui_s'] = sensor.node.eui.split('/')[1]
        s['node_name'] = sensor.node.name
        db_data.append(s)

    response = {
            "draw": request.POST.get('draw'),
            "result": 'ok',
            "data": db_data,
            "recordsFiltered": total,
            "recordsTotal": total
        }
    return JsonResponse(response)