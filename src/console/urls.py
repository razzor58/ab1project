"""Routing http request by uri"""
from django.conf.urls import url
from django.conf.urls import include
from . import views, firmware, tab
from . import api, dt

urlpatterns = [

    # pages
    url(r'^$', views.index, name='index'),
    url(r'^utilities', views.utilities, name='utilities'),
    url(r'^resources', views.resources, name='resources'),

    url(r'^datatables', views.resources_new, name='datatables'),
    url(r'^users', views.users, name='users'),
    url(r'^settings', views.settings, name='settings'),
    url(r'^status', views.status, name='status'),
    url(r'^livelog', views.livelog, name='livelog'),
   
    #forms    
    url(r'^en', views.index_en, name='en'),
    url(r'^form_save', views.form_save, name='form_save'),    
    url(r'^signup', views.signup, name='signup'),
    url(r'^gateway_modal', views.gateway_modal, name='gateway_modal'),        
    url(r'^registration', views.register, name='register'),
    url(r'^registration/complete/$', views.registration_complete, name='registration_complete'),
    url('^', include('django.contrib.auth.urls')),

    # api calls
    url(r'^assign_gateway', api.assign_gateway, name='assign_gateway'),
    url(r'^get_messages_for_console', api.get_messages_for_console, name='get_messages'),
    url(r'^get_chart_data', api.get_chart_data, name='get_chart_data'),  
    url(r'^get_node_data', api.get_node_data, name='get_node_data'),  
    url(r'^get_files_data', api.get_files_data, name='get_files_data'),
    url(r'^get_nodes_sensors', api.get_nodes_sensors, name='get_nodes_sensors'),
    url(r'^edit_node', api.edit_node, name='edit_node'),
    url(r'^delete_node', api.delete_node, name='delete_node'),

    url(r'^send_mqtt', api.send_mqtt_message, name='send_mqtt_message'),

    # # file api 
    # url(r'^upload', api.upload, name='upload'),
    # url(r'^delete', api.delete, name='delete'),

    # datatables 
    url(r'^get_gateway_table', dt.get_gateway_table, name='get_gateway_table'),
    url(r'^get_node_table', dt.get_node_table, name='get_node_table'),
    url(r'^get_sensor_table', dt.get_sensor_table, name='get_sensor_table'),  

    # # firmware call

    url(r'^get_tab_content', tab.tab_dispatcher, name='tab_dispatcher'),  

    url(r'^new_tab', views.new_tab, name='new_tab'),  
    
    # url(r'^smartconfig.bin', firmware.firmware_handler, name='firmware'),
    # url(r'^smartconfig.spiffs.bin', firmware.firmware_handler, name='firmware'),
    # url(r'^firmware.bin', firmware.firmware_handler, name='firmware'),
    # url(r'^update', firmware.firmware_handler, name='firmware'),
    # url(r'^testing', firmware.testing, name='testing'),
]
