from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from . import models
from pytz import all_timezones as all_tz


class SignUpForm(UserCreationForm):
    first_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    last_name = forms.CharField(max_length=30, required=False, help_text='Optional.')
    email = forms.EmailField(max_length=254, help_text='Required. Enter a valid email address.')

    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name', 'email', 'password1', 'password2', )


class ConnectionForm(forms.ModelForm):


    name = forms.TextInput(attrs={'class':'form-control'})

    class Meta:
        model = models.Connection
        exclude = ('id',)
        widgets = {
            'name': forms.TextInput(attrs={'class':'form-control'}),
            'network_type': forms.TextInput(attrs={'class':'form-control'}),
            'delay': forms.TextInput(attrs={'class':'form-control'}),
            'qos': forms.TextInput(attrs={'class':'form-control'}),
            'broker_host': forms.TextInput(attrs={'class':'form-control'}),
            'client_id': forms.TextInput(attrs={'class':'form-control'}),
            'topic_publish': forms.TextInput(attrs={'class':'form-control'}),
            'topic_subscribe': forms.TextInput(attrs={'class':'form-control'}),
            'user_login': forms.TextInput(attrs={'class':'form-control'}),
            'password': forms.PasswordInput(attrs={'class':'form-control'})
        }


class AppSettingsFormTZ(forms.ModelForm):

    class Meta:

        model = models.AppSettings
        exclude = ('id',)
        tz = [(t,t) for t in all_tz]
        widgets = {
            # 'name': forms.TextInput(attrs={'class':'form-control'}),
            # 'rus_name': forms.TextInput(attrs={'class':'form-control'}),
            'value': forms.Select(choices=tz,attrs={'class':'form-control'})
        }

class AppSettingsForm(forms.ModelForm):

    class Meta:

        model = models.AppSettings
        exclude = ('id',)
        # labels = {
        #     'name': _('Writer'),
        # }
        widgets = {
            'name': forms.TextInput(attrs={'class':'form-control'}),
            # 'rus_name': forms.TextInput(attrs={'class':'form-control'}),
            'value': forms.TextInput(attrs={'class':'form-control'})
        }



class DocumentForm(forms.ModelForm):
    class Meta:
        model = models.Firmware
        fields = ('name', )