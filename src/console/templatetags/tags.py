from django import template

register = template.Library()

@register.filter(is_safe=True)
def input_with_classes(value, arg):
    result = value.as_widget(attrs={'class':arg})


@register.filter(is_safe=True)
def add_classes(value):
    cl = 'form-control'
    attrs = {'class': cl }
    return value.label_tag(attrs={'class': 'control-label'}) + value.as_widget(attrs=attrs)


@register.filter(is_safe=True)
def d_items(value):    
    return value.items()


@register.filter(is_safe=True)
def batch(value):
    n = 2
    l = len(value)
    for ndx in range(0, l, n):
        yield value[ndx:min(ndx + n, l)]
