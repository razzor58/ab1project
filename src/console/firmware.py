# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render_to_response
from django.http import HttpResponseRedirect,JsonResponse,HttpResponse
from console import models
import os 
from console import models
import logging


"""
Итого:
при запросах 
/firmware/smartconfig.bin
/firmware/firmware.bin
Просто отдаем файл, помещенный "самым свежим"


/firmware/smartconfig.spiffs.bin
Отдаем его, только если хэш на устройстве совпадает с хэшэм "самой свежей" прошивки smartconfig.latest.bin
иначе игнорируем запрос
 
такая работа сервера будет правильной? 
"""
log = logging.getLogger('web')


def search_latest(request):
    #Логируем в файл
    log.info(request.META)

    # Читаем заголовки
    mode = request.META.get('HTTP_X_ESP8266_MODE')
    node_version_name = request.META.get('HTTP_X_ESP8266_VERSION','.0')
    node_version_hash = request.META.get('HTTP_X_ESP8266_SKETCH_MD5','no hash')
    node_mac = request.META.get('HTTP_X_ESP8266_AP_MAC','no mac') 
    url = request.META.get('PATH_INFO','no url') 

    # Опредеим тип прошивки для поиска
    if mode == 'spiffs' and url == '/update/':
        firmware_full_type = 'smartconfig' 
    elif mode == 'spiffs':
        firmware_full_type = 'smartconfig.spiffs' 
    elif 'smartconfig' in url:
        firmware_full_type = 'smartconfig' 
    else:
        firmware_full_type = 'firmware' 

    # персональная прошивка
    latest = models.Firmware.objects.filter(is_latest=1, firmware_type=firmware_full_type, node_model__name=node_version_name)

    # Если не нашли индивидуальной прошивки ищем общую
    if latest.count() > 0:
        server_latest = latest[0]
        response_data = server_latest.file_hash + ": " + server_latest.name

    else:
        # server_latest = models.Firmware.objects.filter(is_latest=1,firmware_type=firmware_full_type)[0]
        server_latest = None
        response_data = 'firmware not found'

    # Переменнные для логирования в БД
    request_meta = ";".join([k + ":" + v for k, v in request.META.items() if 'HTTP_X' in k ])
    request_data = node_mac + ": " + str(node_version_name) + ": " + str(node_version_hash)
    
    # логируем запрос в БД 
    m = models.FirmwareLog(request_data=request_data, request_meta=request_meta, responce_data=response_data, method=url)
    m.save()

    return server_latest

def firmware(request):

    server_latest = search_latest(request)

    # Имя файла и как будет отдаваться по-умолчанию
    filename = server_latest.name
    verb_name = '{}.latest.bin'.format(server_latest.firmware_type)

    # Передаем файл в ответе. Не работает в IE
    filename = os.path.join('..','firmwares',filename)
    with open(filename, 'rb') as ex:
        response = HttpResponse(ex.read(), content_type='application/force-download')
    response['Content-Disposition'] = 'attachment; filename=' + verb_name
    
    return response


def update(request):

    server_latest = search_latest(request)

    node_version_name = request.META.get('HTTP_X_ESP8266_VERSION','.0')
    node_version_hash = request.META.get('HTTP_X_ESP8266_SKETCH_MD5','no hash')


    if server_latest.file_hash == node_version_hash:
       code = 304 
       text = "node version up to date"

    else:
        code = 200
        text = "need to update firmware"

    result = { "result ": text, 
            "last version: ": server_latest.name + ": " + server_latest.file_hash, 
            "node_version": node_version_name + ": " + node_version_hash
            }

    return JsonResponse(result, safe=False,content_type="application/json",status=code)


def firmware_handler(request):

    # Параметры устройства
    node_version_name = request.META.get('HTTP_X_ESP8266_VERSION','.0')
    node_version_hash = request.META.get('HTTP_X_ESP8266_SKETCH_MD5','no hash')

    # Ищем последнюю прошивку
    server_latest = search_latest(request)

    if server_latest is None:
        code = 404 
        text = "firmware not found"
        return JsonResponse(text, safe=False,content_type="application/json",status=code)

    elif server_latest.file_hash == node_version_hash:
        code = 304 
        text = "node version up to date"
        return JsonResponse(text, safe=False,content_type="application/json",status=code)

    else:
        if request.META.get('PATH_INFO') == '/update/':
            code = 200 
            text = "New version available"
            return JsonResponse(text, safe=False,content_type="application/json",status=code)

        else:
            filename = server_latest.name
            verb_name = '{}.latest.bin'.format(server_latest.firmware_type)

            # Передаем файл в ответе. Не работает в IE
            filename = os.path.join('..','firmwares',filename)
            with open(filename, 'rb') as ex:
                response = HttpResponse(ex.read(), content_type='application/force-download')
            response['Content-Disposition'] = 'attachment; filename=' + verb_name
            
            return response


def testing(request):

    text="your request has been saved"

    device_name = request.META.get('HTTP_X_ESP8266_DEVICE_NAME') 
    version = request.META.get('HTTP_X_ESP8266_VERSION') 
    test_number = request.META.get('HTTP_X_ESP8266_TEST_NUMBER') 
    sta_mac = request.META.get('HTTP_X_ESP8266_STA_MAC') 
    sketch_md5 = request.META.get('HTTP_X_ESP8266_SKETCH_MD5') 

    # логируем запрос в БД 
    m = models.TestingLog(device_name=device_name, version=version,test_number=test_number, sta_mac=sta_mac, sketch_md5=sketch_md5)
    m.save()

    return JsonResponse(text, safe=False,content_type="application/json",status=200)