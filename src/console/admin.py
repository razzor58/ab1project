# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.contrib import admin
from django.utils import timezone
# Register your models here.
from .models import Client
from .models import Connection, Node, Gateway, Sensor, SensorData,AppSettings,SensorActualData
from .models import City, HeartbeatTimer
from .models import MQTTMessage
from .models import NodeBatteryUsage
from .models import Firmware, FirmwareLog
from .models import NodeModels
from .models import TestingLog, ApiTypePayloadCommand, ApiTypeSensorCommand, ApiTypeDataStreamCommand
from .models import DeviceCommand,NodeCommand,MenuGatewayCommand,InternalCommand,ApiMessageTypeCommand

class HeartbeatTimerAdmin(admin.ModelAdmin):
    
    def date_format(self, obj):           
        tz_date = timezone.localtime(obj.last_check_date)
        return tz_date.strftime("%Y-%m-%d %H:%M:%S")

    list_display = ('object_type','date_format')

class AppSettingsAdmin(admin.ModelAdmin):
    list_display = ('rus_name','value')
    # list_display = ('name','name')

class ClientAdmin(admin.ModelAdmin):
    list_display = ('name', 'email')


class ConnectionAdmin(admin.ModelAdmin):
    list_display = ('name', 'broker_host')


class NodeAdmin(admin.ModelAdmin):    
   
    ordering = ('-gateway','-eui')


    def reg_date_format(self, obj):           
        tz_date = timezone.localtime(obj.reg_date)
        return tz_date.strftime("%Y-%m-%d %H:%M:%S")   

    readonly_fields = ('reg_date_format',)

    def send_status_format(self, obj):           
        tz_date = timezone.localtime(obj.send_status)
        return tz_date.strftime("%Y-%m-%d %H:%M:%S")    

    send_status_format.short_description = 'send_status'    

    list_display = ('gateway','eui','parent','name','send_status_format','library','firmware_min')

class GatewayAdmin(admin.ModelAdmin):

    def reg_date_format(self, obj):           
        tz_date = timezone.localtime(obj.reg_date)
        return tz_date.strftime("%Y-%m-%d %H:%M:%S") 

    readonly_fields = ('reg_date_format',)

    list_display = ('reg_date_format','name', 'client','connection','inclusion_mode')


class SensorAdmin(admin.ModelAdmin):
    # def node_type(self, obj):
    #     return obj.node.node_type
    model = Sensor
 
    def reg_date_format(self, obj):           
        tz_date = timezone.localtime(obj.reg_date)
        return tz_date.strftime("%Y-%m-%d %H:%M:%S") 

    readonly_fields = ('reg_date_format',)

    list_display = ['reg_date_format','node_eui','node_name','child_sensor_id','node_type','name']         


    def node_eui(self, obj):
        return str(obj.node)

    def node_name(self, obj):
        return obj.node.name

class SensorDataAdmin(admin.ModelAdmin):
    # def node_type(self, obj):
    #     return obj.node.node_type
    model = SensorData

    list_display = ['sensor','receive_date','variable_type','value']         



class CityAdmin(admin.ModelAdmin):
    list_display = ('name', 'rus_name','short_name','timezone')



class MQTTMessageAdmin(admin.ModelAdmin):
    list_display = ('pk','timestamp', 'uri','routing_key', 'payload')


class NodeBatteryUsageAdmin(admin.ModelAdmin):
    list_display = ('timestamp', 'node','level')


class FirmwareAdmin(admin.ModelAdmin):

    def send_status_format(self, obj):           
        tz_date = timezone.localtime(obj.uploaded_at)
        return tz_date.strftime("%Y-%m-%d %H:%M:%S")    


    list_display = ('name','file_hash','firmware_type','is_latest','file_type','node_model', 'send_status_format')


class FirmwareLogAdmin(admin.ModelAdmin):

    def send_status_format(self, obj):           
        tz_date = timezone.localtime(obj.timestamp)
        return tz_date.strftime("%Y-%m-%d %H:%M:%S")    

    list_display = ('send_status_format','method','request_data','responce_data')

class NodeModelsAdmin(admin.ModelAdmin):
    list_display = ('name',)


class TestingLogAdmin(admin.ModelAdmin):

    def send_status_format(self, obj):           
        tz_date = timezone.localtime(obj.timestamp)
        return tz_date.strftime("%Y-%m-%d %H:%M:%S")    

    list_display = ('send_status_format','device_name','test_number','version','sta_mac','sketch_md5')


class SensorActualDataAdmin(admin.ModelAdmin):
    # def node_type(self, obj):
    #     return obj.node.node_type
    model = SensorActualData

    list_display = ['sensor','receive_date','variable_type','value']         



class ApiMessageTypeCommandAdmin(admin.ModelAdmin):
    # def node_type(self, obj):
    #     return obj.node.node_type
    model = ApiMessageTypeCommand

    # list_display = ['sensor','receive_date','variable_type','value']    

class MenuGatewayCommandAdmin(admin.ModelAdmin):

    def str_obj(self, obj):
        return str(obj)

    str_obj.short_description = 'gateway_command'   

    list_display = ['str_obj','internal_command','broadcast','c_internal','ask','i_discover_request','qos','retained']  
    model = MenuGatewayCommand


class ApiTypePayloadCommandAdmin(admin.ModelAdmin):
    model = InternalCommand

class ApiTypeSensorCommandAdmin(admin.ModelAdmin):
    model = InternalCommand

class ApiTypeDataStreamCommandAdmin(admin.ModelAdmin):
    model = InternalCommand

admin.site.register(MenuGatewayCommand,MenuGatewayCommandAdmin)
admin.site.register(ApiMessageTypeCommand,ApiMessageTypeCommandAdmin)
admin.site.register(ApiTypePayloadCommand,ApiTypePayloadCommandAdmin)
admin.site.register(ApiTypeSensorCommand,ApiTypeSensorCommandAdmin)
admin.site.register(ApiTypeDataStreamCommand,ApiTypeDataStreamCommandAdmin)



admin.site.register(SensorActualData, SensorActualDataAdmin)
admin.site.register(TestingLog, TestingLogAdmin)
admin.site.register(NodeModels, NodeModelsAdmin)
admin.site.register(Firmware, FirmwareAdmin)
admin.site.register(FirmwareLog, FirmwareLogAdmin)
admin.site.register(NodeBatteryUsage, NodeBatteryUsageAdmin)
admin.site.register(HeartbeatTimer, HeartbeatTimerAdmin)
admin.site.register(AppSettings, AppSettingsAdmin)
admin.site.register(MQTTMessage, MQTTMessageAdmin)
admin.site.register(City, CityAdmin)
admin.site.register(Client, ClientAdmin)
admin.site.register(Connection, ConnectionAdmin)
admin.site.register(Node, NodeAdmin)
admin.site.register(Gateway, GatewayAdmin)
admin.site.register(Sensor, SensorAdmin)
admin.site.register(SensorData, SensorDataAdmin)
