# -*- coding: utf-8 -*-
"""Module for api methods."""
from datetime import timedelta,datetime
from django.utils import timezone
import pytz
import json
import os
from .models import MQTTMessage
import django.utils.timezone as timezone
from django.core import serializers
from django.http import JsonResponse
from django.http import HttpResponse
from django.shortcuts import redirect
import hashlib
from .forms import DocumentForm
from rabbitparser.producer import p


from .forms import ConnectionForm
from .models import Gateway, Sensor,Node, Client, SensorData, AppSettings,MenuGatewayCommand
from collections import defaultdict
from django.core.files.storage import FileSystemStorage

def save_connection(request):
    """Save connection form"""
    form = ConnectionForm(request.POST)
    if request.method == 'GET':
        return JsonResponse({'err': 'POST method required'}, content_type="application/json")
    else:
        if form.is_valid():
            form.save()
        else:
            return JsonResponse({'err': 'Ошибка валидации', 'messages': form.errors},\
            content_type="application/json")
    return JsonResponse({'err': 'no'})

def get_messages_for_console(request):
    """get last records from console_rawmessage table"""
    query = {
        'timestamp__gt': timezone.now() - timedelta(minutes=3),
        'pk__gt': request.GET.get('max_id')
    }

    tz_name = AppSettings.objects.get(name='timezone_for_console').value or 'Europe\Moscow'
    tz = pytz.timezone(tz_name)
    res = []
    for d in MQTTMessage.objects.filter(**query).values():
        t = d['timestamp']
        d['timestamp'] = (t + tz.utcoffset(datetime.now())).strftime('%Y-%m-%d %H:%M:%S')
        res.append({'pk':d['id'],'fields':d})
    return HttpResponse(json.dumps(res), content_type="application/json")

def save_gateway(mac):

    name='gateway-' + mac

    if Gateway.objects.filter(name=name).count() == 0:
        gw = Gateway(name=name)
        gw.save()
        g_id = gw.id
    else:
        g_id = Gateway.objects.get(name=name).id

    return g_id

def assign_gateway(request):

    # read post params
    gate = request.POST.get('g_id')    
    client = request.POST.get('c_id')

    # Set gate to client
    g = Gateway.objects.get(name=gate)
    g.client = Client.objects.get(pk=int(client))
    g.save()

    # select results
    gws = Gateway.objects.filter(client__pk=client)
    nodes = Node.objects.filter(gateway__in=gws)
    sensors = Sensor.objects.filter(node__in=nodes).count()
    data = {'gws': gws.count(), 
            'nodes': nodes.count(),
            'sensors': sensors 
            }
    return JsonResponse(data, content_type="application/json")



def get_chart_data(request):
    result = []
    node = Node.objects.get(eui=request.POST.get('node_eui',''))
    minutes_ago=request.POST.get('minutes_ago',120)
    minutes_ago_value = timezone.now()-timedelta(minutes=minutes_ago)
    sensors = Sensor.objects.filter(node=node)
    rows = SensorData.objects.filter(sensor__in=sensors,receive_date__gte=minutes_ago_value)
    data = defaultdict(list)
    
    for row in rows:
        data[row.sensor].append((row.receive_date,row.variable_type,row.value,row.id))
    for k, v in data.items():
        chart_data = {
            'id': [i[3] for i in v]
            ,'name': str(k).replace('/','')
            ,'time': [i[0].strftime('%H:%M:%S') for i in v]
            ,'var_type': [i[1] for i in v]
            ,'value': [i[2] for i in v]
            
        }
        result.append(chart_data)
    # chart_data={}
    return JsonResponse(result, safe=False,content_type="application/json")



def get_node_data(request):
    result = [n.eui for n in Node.objects.all()]

    return JsonResponse(result, safe=False,content_type="application/json")


def get_nodes_sensors(request):

    node = Node.objects.get(eui=request.POST.get('node_eui',''))
    result = [s.name.replace('/','') for s in Sensor.objects.filter(node=node)]

    return JsonResponse(result, safe=False,content_type="application/json")


def get_files_data(request):
    path = os.path.join('..','firmwares')
    lst = os.listdir(path)
    result = [{
                'text' : 'Firmwares',
                'state' : {
                    'opened' : 'true'
                    
                },
                'icon':'fa fa-folder',
                'children' : [{ 'text' : str(i),  "icon" : "jstree-file" } for i in lst]
    }]


    return JsonResponse(result, safe=False,content_type="application/json")



def upload(request):
    if request.method == 'POST':
    
        form = DocumentForm(request.POST, request.FILES)
        firmware = form.save(commit=False)

        # calculate hash
        form_file = request.FILES['document']
        hash_md5 = hashlib.md5()
        with form_file as f:
            for chunk in iter(lambda: f.read(4096), b""):
                hash_md5.update(chunk)

            fs = FileSystemStorage(location=os.path.join('..','firmwares'))
            filename = fs.save(f.name, f)

        if 'smartconfig' in form_file.name:
            firmware.firmware_type = 'smartconfig'
        elif 'firmware' in form_file.name:
            firmware.firmware_type = 'firmware'
        else:
            firmware.firmware_type = 'incorrect filename'

        firmware.name = form_file.name
        firmware.file_size = form_file.size
        firmware.file_hash = hash_md5.hexdigest()
        firmware.uploaded_by = request.user
        firmware.save()

        # simple save
        # fs = FileSystemStorage(location=os.path.join('..','firmwares'))
        # filename = fs.save(form_file.name, form_file)

        return redirect('utilities')
    else:
        result = { "result": "POST method required" }
        return JsonResponse(result, safe=False,content_type="application/json")
    


def delete(request):
    if request.method == 'POST':
        form_file = request.POST['filename']
        fs = FileSystemStorage(location=os.path.join('..','firmwares'))
        filename = fs.delete(form_file)
        return redirect('utilities')
    else:
        result = { "result": "POST method required" }
        return JsonResponse(result, safe=False,content_type="application/json")


def send_mqtt_message(request):
    device = "-".join(request.POST.get('device').split('-')[0:2])
    command_id = request.POST.get('message')

    m = MenuGatewayCommand.objects.get(pk=command_id)
    fields = [m.internal_command,m.broadcast,m.c_internal,m.ask,m.i_discover_request]
    message = ".".join(str(f)for f in fields)

    if device is None or message is None:
        result = { "result": "missing parameters" }
        code = 200
    
    else:
        full_mqtt = device + '-in.' + message
        print(full_mqtt)
        p.publish(full_mqtt,'test')
        result = { "result": "publish completed" }
        code=200
    return JsonResponse(result,safe=False,content_type="application/json")


def edit_node(request):
    data = {'result': 'ok'}
    eui = request.POST.get('eui')
    try:
        node = Node.objects.get(eui=eui)
        node.firmware_min = request.POST.get('firmware_min')
        node.parent = request.POST.get('parent')
        node.node_type = request.POST.get('node_type')
        node.save()
    except Exception as e:
        data['result'] = str(e)
    
    return JsonResponse(data,safe=False,content_type="application/json")


def delete_node(request):
    data = {'result': 'ok'}
    eui = request.POST.get('eui')
    try:
        Node.objects.filter(eui=eui).delete()
    except Exception as e:
        data['result'] = str(e)
    
    return JsonResponse(data,safe=False,content_type="application/json")
    
    return JsonResponse(result,safe=False,content_type="application/json")

