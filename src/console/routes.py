"""Routing http request by uri"""
from django.conf.urls import url
from django.conf.urls import include
from . import api,firmware

urlpatterns = [
    # firmware call
    url(r'^smartconfig.bin', firmware.firmware_handler, name='firmware'),
    url(r'^smartconfig.spiffs.bin', firmware.firmware_handler, name='firmware'),
    url(r'^firmware.bin', firmware.firmware_handler, name='firmware'),
    url(r'^update', firmware.firmware_handler, name='firmware'),
    url(r'^testing', firmware.testing, name='testing'),

    #file api 
    url(r'^upload', api.upload, name='upload'),
    url(r'^delete', api.delete, name='delete'),
]
