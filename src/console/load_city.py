import csv
import sys 
import django
import os
# For use django app 
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)),'..'))
os.environ['DJANGO_SETTINGS_MODULE'] = 'web.settings'
django.setup()
from console import models
# from models import City

with open('c.csv', newline='') as csvfile:
     spamreader = csv.reader(csvfile, delimiter=';', quotechar='|')
     for r in spamreader:
         print(', '.join(r))
         rus = None
         for i in reversed(r[2].split(',')):
             if '?' not in i and rus is None and 'i' not in i:
                rus = i
                break

         print(', '.join(r))
         s = models.City(name=r[1],rus_name=rus,timezone=r[7],latitude=r[3],longitude=r[4],population=r[6])
         s.save()
