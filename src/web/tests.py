from django.test import TestCase
import sys 
import shutil
import requests
import os

sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)),'..','rabbitparser'))
from rabbitparser import mqtt
from console import models


class ParserTestCase(TestCase):


    def test_create_gateway(self,mqtt_string=None):
        print('-'*70)
        """Object created when MQTT message appear"""
        rk = mqtt_string or 'test-GVKAFPA1230-out.8.2.1.0.0.90'

        data = dict(
                 mac=rk.split('-')[1]            
                ,node_id=int(rk.split('.')[1])
                ,child_sensor_id=int(rk.split('.')[2])
                ,command=int(rk.split('.')[3])
                ,ask=int(rk.split('.')[4])
                ,type=int(rk.split('.')[5])
                ,payload=rk.split('.')[6]
                ,direction=rk.split('-')[2].split('.')[0]
                ,full_message=".".join(rk.split('.')[:-1])
                )

        message = mqtt.Message(**data)
        message.process()
        gateway = models.Gateway.objects.filter(name='gateway-{}'.format(data['mac']))
        print('Procedeed {}'.format(rk))
        self.assertEqual(gateway.count(), 1)



class FirmwareUploadTest(TestCase):

    def test_web_response(self):
        print('-'*70)
        mode = sys.argv[1] if len(sys.argv) > 1 else 'upd'

        urls = {
            'spiffs':'http://fwupdate.hohobo.com/firmware/smartconfig.spiffs.bin'
            ,'bin': 'http://fwupdate.hohobo.com/firmware/firmware.bin'
            ,'smart': 'http://fwupdate.hohobo.com/firmware/smartconfig.bin'
            ,'upd': 'http://fwupdate.hohobo.com/update/'
            ,'test': 'http://fwupdate.hohobo.com/testing/'
        }

        url = urls[mode]

        headers = {
            'X-ESP8266-VERSION':'HO-Gate HWG-100'
            ,'X-ESP8266-AP-MAC':'Unit Test'
            ,'X-ESP8266-DEVICE-NAME':'Unit Test'
            ,'X-ESP8266-TEST-NUMBER':'1'
            ,'X-ESP8266-STA-MAC':'5C:CF:7F:86:44:7E'
            ,'X-ESP8266-CHIP-SIZE':'4194304'
            ,'X-ESP8266-FREE-SPACE':'2830336'
            ,'X-ESP8266-SKETCH-SIZE':'311344'
            ,'X-ESP8266-SDK-VERSION':'2.1.0(7106d38)'
            ,'X-ESP8266-SKETCH-MD5':'efd88617e892c45b95e742e2e7f0ce2a'
            ,'X-ESP8266-MODE':'sketch'
        }

        s = requests.Session()
        s.headers.update(headers)

        print(url)
        if mode =='upd':
            response = s.get(url)
            print(str(response.status_code) + ":" +  response.text)
        
        else:    
            name = os.path.join('..','firmwares',url.split('/')[-1])
            response = s.get(url,
                stream=True
            )
            if int(response.headers.get('Content-Length',0)) > 1000:
                with open(name, 'wb') as out_file:
                    shutil.copyfileobj(response.raw, out_file)
                for f in os.scandir(os.path.join('..','firmwares')):
                    print(f.name + "..." + str(f.stat().st_size))
            else:
                print(str(response.status_code) + ":" +  response.text)

        self.assertNotEqual(response.status_code, 500)