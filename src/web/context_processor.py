from . import settings
from datetime import datetime


def app_version(request):
    
    context = { 
        'app_version': settings.APP_VERSION 
        ,'current_year': datetime.today().year
    }
    
    return context 
