"""
Django settings for web project.

Generated by 'django-admin startproject' using Django 1.11.5.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.11/ref/settings/
"""

import os
import sys

ROTATE_LOGFILE = 'logging.FileHandler' if sys.platform =='win32' else 'logging.handlers.TimedRotatingFileHandler'


# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.11/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'j1)aclclrp+$pb55ta!6!_8t^0n)e(n)#wk%mlenzn2@i$z$_s'

# SECURITY WARNING: don't run with debug turned on in production!
if sys.platform == 'win32':
    DEBUG = True
    log_settings = {
            'level': 'DEBUG',
            'class': ROTATE_LOGFILE,
            'filename': os.path.join(BASE_DIR,'..','logs', 'application.log'),
			'formatter': 'verbose'
    }
else:
    DEBUG = True
    log_settings = {
            'level': 'DEBUG',
            'class': ROTATE_LOGFILE,
            'when': 'D',
            'filename': os.path.join(BASE_DIR,'..','logs', 'application.log'),
			'formatter': 'verbose'
    }

ALLOWED_HOSTS = ['185.158.155.190','127.0.0.1','hohobo.ru','hohobo.com','fwupdate.hohobo.com']


# Application definition

INSTALLED_APPS = [
    'console.apps.ConsoleConfig',
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    # 'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'web.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
                'web.context_processor.app_version',
            ],
        },
    },
]

WSGI_APPLICATION = 'web.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.11/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'fox',
        'USER': 'postgres',
        'PASSWORD': 'psql@pwd123',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}

#LOGGING
LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
	'formatters': {
        'verbose': {
            'format': '%(asctime)s %(levelname)s %(message)s'
        }
	},
    'handlers': {
        'file': {
            'level': 'DEBUG',
            # 'class': 'logging.handlers.TimedRotatingFileHandler',
            'class': ROTATE_LOGFILE,
            # 'when': 'D',
            'filename': os.path.join(BASE_DIR,'..','logs', 'debug.log'),
			'formatter': 'verbose'
        },
        'web': log_settings,
        # {
        #     'level': 'DEBUG',
        #     'class': ROTATE_LOGFILE,
        #     # 'when': 'D',
        #     'filename': os.path.join(BASE_DIR,'..','logs', 'application.log'),
		# 	'formatter': 'verbose'
        # },
    },
    'loggers': {
        'django': {
            'handlers': ['file'],
            'level': 'DEBUG',
            'propagate': True,
        },
        'web': {
            'handlers': ['web'],
            'level': 'DEBUG',
            'propagate': True,
        },
    },
}

# Password validation
# https://docs.djangoproject.com/en/1.11/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/1.11/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'Asia/Krasnoyarsk'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.11/howto/static-files/

STATIC_URL = '/static/'
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
)


MEDIA_URL = '/firmwares/'
MEDIA_ROOT = os.path.join(BASE_DIR, '..' ,'firmwares')

try:
    with open('version.md') as f:
        APP_VERSION = f.read()
except Exception:
    APP_VERSION = 'undefined'