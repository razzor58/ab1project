#!/usr/local/fox/env/bin/python
import pika
from configparser import ConfigParser
import os
import time
class Producer(object):

    def __init__(self):

        current_dir = os.path.abspath(os.path.dirname(__file__))
        config = ConfigParser()
        config.read(os.path.join(current_dir,'config.ini'))        

        # Read params from config
        url = config['rabbitmq']['url']
        self.exchange_name = config['rabbitmq']['exchange']

        # headers = { 'x-mqtt-dup': False,
        #             'x-mqtt-publish-qos': 0,
        #             'x-received-from': [
        #                 {
        #                 'uri': 'amqp://backend.hohobo.com',
        #                 'cluster-name': 'backend@mqtt'
        #                 }
        #             ]}
        headers={}            

        self.properties = pika.BasicProperties(headers=headers)

        # Create rabbitmq connection
        self.connection = pika.BlockingConnection(pika.URLParameters(url))

        self.channel = self.connection.channel()

        self.channel.exchange_declare(exchange=self.exchange_name, exchange_type='topic',durable=True)

    def publish(self,routing_key, message):
        time.sleep(0.01)
        self.channel.basic_publish(exchange=self.exchange_name,
                            routing_key=routing_key,
                            body=message
                            ,properties=self.properties)


p = Producer()
