#!/usr/local/fox/env/bin/python
import pika
import os
import time
import logging
from configparser import ConfigParser
import db
import sys
import os
import django
import traceback

# For use django app
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)),'..'))
os.environ['DJANGO_SETTINGS_MODULE'] = 'web.settings'
django.setup()
from mqtt import Message


# Define helper objects
logger = logging.getLogger('rabbit_parser')
logger.setLevel(logging.DEBUG)
# create file handler which logs even debug messages
fh = logging.FileHandler(os.path.join(os.path.abspath(os.path.dirname(__file__)),'rabbit_parser.log'))
fh.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(lineno)d - %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
# add the handlers to the logger
logger.addHandler(fh)
logger.addHandler(ch)
config = ConfigParser()
# config.read('config.ini')
database = db.Helper()

# Read params from config
# url = config['rabbitmq']['url']
# exchange = config['rabbitmq']['exchange']
# routing_key = config['rabbitmq']['routing_key']
url="amqp://webuserbackend:webuserbackend321@hohobo.com:5672/%2f"
exchange="amq.topic"
routing_key="#"

# Create rabbitmq connection
connection = pika.BlockingConnection(pika.URLParameters(url))

# Create rabbit channel
channel = connection.channel()

# Generate rabbitmq queue name
q_name = 'backend_parser_' + sys.platform
queue_name = channel.queue_declare(exclusive=True,queue=q_name).method.queue
# Subsribe on queue messages
channel.queue_bind(exchange=exchange,
                   queue=queue_name,
                   routing_key=routing_key
                  )

# what to do when message reveive from rabbit
def callback(ch, method, properties, body):
    """
        What to do when message arrived
        params:
        consumer_callback(channel, method, properties, body)
        channel: BlockingChannel
        method: spec.Basic.Deliver
        properties: spec.BasicProperties
        body: str or unicode
        consumer_tag, delivery_tag, exchange, redelivered, uri, cluster_name,
            routing_key,payload, gateway, direction, data1, data2, data3, data4,data5
    """
    try:
        receive_from = properties.headers.get('x-received-from') or [{}]
        rk = 'test-111111-in.0.0.0.0.0' if method.routing_key == q_name else method.routing_key
        # Example:
        # rk = gateway-1870439-out.1.255.3.0.21 
        #      gateway-5CCF7F86447E-out.0.0.1.0.37           
        direction = rk.split('-')[2].split('.')[0]
        full_message = rk.split('.')[1:]
        receive_from = properties.headers.get('x-received-from') or [{}]
        broker_host= receive_from[0].get('uri','hohobo.com')
        #print(rk +' : ' + body.decode('utf-8'))
        if direction:
            data = dict(
                mac=rk.split('-')[1]            
                ,node_id=int(rk.split('.')[1])
                ,child_sensor_id=int(rk.split('.')[2])
                ,command=int(rk.split('.')[3])
                ,ask=int(rk.split('.')[4])
                ,type=int(rk.split('.')[5])
                ,payload=body.decode('utf-8')
                ,full_message=full_message
                ,broker_host = broker_host
                ,direction=direction
                )
            message = Message(**data)
            message.process()            

    except Exception as e:
        logger.error(e, exc_info=True)
        logger.error("problem method:{0}".format(method))
# Define confumer object
channel.basic_consume(callback,
                      queue=queue_name,
                      no_ack=True)
# Start listener
# import daemon
# logger.info('start')
channel.start_consuming()
