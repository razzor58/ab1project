# -*- coding: utf-8 -*-
"""Module for store mqtt message class"""
from console.models import Gateway, Node, Sensor,SensorData, Connection, NodeBatteryUsage, AppSettings, SensorActualData
from mqtt_types import SensorType,PayloadTypes,InternalTypes
import logging
from django.utils import timezone
import datetime
from producer import p
from producer import Producer
import pika
import pytz
import calendar

class Message(object):
    """Class for understand and process received data"""

    def __init__(self,**data):   
        self.p = Producer()
        self.mac=data.get('mac',0)                
        self.node_id=data.get('node_id',0)
        self.child_sensor_id=data.get('child_sensor_id',0)
        self.command=data.get('command',0)
        self.ask=data.get('ask',0)
        self.type=data.get('type',0)
        self.payload=data.get('payload',0)
        self.direction = data.get('direction','')

        # Surrogat names
        self.gateway_name = 'gateway-' + str(self.mac)
        self.node_name = 'gtw-DEV-' + str(self.mac)

        # Database Objects
        self.gateway = None
        self.node = None
        self.sensor= None
        self.connection = None
        
        self.is_gataway_message = True if self.child_sensor_id == 255 else False
        self.log = logging.getLogger('rabbit_parser')
        
        self.full_message=data.get('full_message')
        self.process_flow=''
        self.broker_host = data.get('broker_host')        
        self.node_eui = 'gateway-' + self.mac + '-out/' + str(self.node_id)        
        self.ans_routing_key = self.gateway_name + '-in.' +  ".".join(self.full_message)


    def publish(self, rk, message):
        try:
            self.p.publish(rk, message)
        except Exception as e:
            self.log.error(e, exc_info=True)
            self.p = Producer()
            self.p.publish(rk, message)

    def do_presentation(self):        
        if self.is_gataway_message:                   
            """
            # гетвей
            # gateway-78945612-out/0/255/0/0/18 payload: 2.2.0 - Library
            # /17 S_ARDUINO_NODE /18 S_ARDUINO_REPEATER_NODE    - Node type        
            # Сенсоры
            # gateway-78945612-out/0/2/0/0/21 payload: HEALTHY
            # Name: HEALTHY # Value: 1            
            """
            if self.type == 17:
                self.node.library = self.payload
                self.node.node_type = 'Node'
                self.node.save()

            if self.type == 18:
                self.node.library = self.payload
                self.node.node_type = 'Repeater Node'
                self.node.save()
        else:
            self.sensor.name = self.payload
            self.sensor.value = 1
            self.sensor.node_type = SensorType(self.type).name
            self.sensor.save()

    def do_set(self):         
        # issue 12
        if self.is_gataway_message is False:  
            var_type = PayloadTypes(self.type).name
            sensor_data = SensorData(sensor=self.sensor,variable_type=var_type)      
            sensor_data.value = self.payload
            sensor_data.save()

            # Save last value for admin page
            try:
                actual_data, ad_created = SensorActualData.objects.get_or_create(sensor=self.sensor,variable_type=var_type)
                actual_data.value = self.payload
                actual_data.receive_date = timezone.now()
                actual_data.save()
            except Exception as e:
                self.log.error("Unable to save SensorActualData") 

            if self.sensor.node_type == 'S_SOUND' and var_type == 'V_VAR5':
                self.node.rssi = self.payload
                self.node.save()

        # issue 12
        # if self.type == 37:
        #     self.node.rssi = self.payload
        #     self.node.save()

    def do_req(self):
        pass

    def do_internal(self):
        """
        gateway-78945612-out/0/255/3/0/22 payload: 1507646          - node.status_seen:
        gateway-78945612-out/0/255/3/0/12 payload: 1.0.0.e51.rf24   - Firmware min
        gateway-78945612-out/0/255/3/0/11 payload: gtw-DEV-78945612 node-name
        gateway-5CCF7F1C8A67-out/0/255/3/0/0 payload: 16  -Bat level

        gateway-5CCF7F86447E-out/2/255/3/0/1
        вот так должен ответить ноду
        gateway-5CCF7F86447E-in/2/255/3/0/1 payload: 1514457948
        """       
        
        if self.type == 3 and self.direction == 'out':
            # Функция присвоения номера узлу. Выдается первый сводобный номер по порядку в пределах GW
            # Описание: https://gitlab.com/oleggreengo/Back-end/issues/2  
            INCLUSION_MODE = None
            try:
                INCLUSION_MODE = AppSettings.objects.get(name="INCLUSION_MODE").value
            except Exception:
                self.log.error("Unable to get INCLUSION_MODE setting") 
              
            if (INCLUSION_MODE == 'ON' and self.gateway.inclusion_mode == 1) or INCLUSION_MODE != 'ON' or INCLUSION_MODE is None:
                all_nums = []
                i = 1
                node_new_id = None
                for node in Node.objects.filter(gateway=self.gateway):
                    if node.public_id:
                        all_nums.append(node.public_id)
                while node_new_id is None or (node_new_id is None and i < 255):
                    if i not in all_nums:
                        node_new_id = i
                    i = i + 1
                if node_new_id is None:
                    self.log.error('Can not set id for {}'.format(self.full_message))    
                else:   
                    lst = self.ans_routing_key.split('.')[:-1]
                    lst.append('4')
                    rk = ".".join(lst)
                    #p.publish(rk,str(node_new_id))
                    self.log.info(rk + ": " + str(node_new_id))
                    try:
                        self.p.publish(rk,str(node_new_id))
                    except Exception:
                        self.p = Producer()
                        self.p.publish(rk,str(node_new_id))

        if self.is_gataway_message and self.direction == 'out':                           

            if self.type == 0:
                self.node.bat_level = self.payload                
                self.node.save()  
                s = NodeBatteryUsage(node=self.node, level= self.payload)
                s.save()

            if self.type == 1:       
            # Функция установки времени на устройстве:
            # Устройство не работает с timezone, поэтому устанавливается 
            # локальное время на основе местонахождения клиента                
                try:
                    tz = pytz.timezone(self.node.gateway.client.city.timezone)
                except Exception:
                    self.log.error("Client TimeZone not found. Set UTC for" + str(self.full_message))
                    tz = pytz.utc  

                utc = datetime.datetime.utcnow()
                local = tz.utcoffset(utc)
                time_for_device = calendar.timegm((utc+local).timetuple())         
                
                self.log.info(self.ans_routing_key + ": "+ str(time_for_device))
                self.publish(self.ans_routing_key,str(time_for_device))
      
            if self.type == 5:
                self.gateway.inclusion_mode = self.payload
                self.gateway.save()
            if self.type == 11:
                self.node.name = self.payload
                self.node.save()
            if self.type == 12:
                self.node.firmware_min = self.payload                
                self.node.save()
            if self.type == 21:                
                self.node.parent = 0 if self.node_id == 0 else self.payload
                self.node.save()                
            if self.type == 22:
                self.node.status_seen = str(self.payload)
                self.node.save()
           

    def do_stream(self):
        pass

    def save_devices(self):
        """ 
            Define devices.
            If devices is unknown - create it into database.
        """
        #Connections
        self.connection, c_created = Connection.objects.get_or_create(name=self.gateway_name,            
            network_type ='MQTT', topic_publish = self.gateway_name + '-in',
            topic_subscribe = self.gateway_name + '-out')
        if c_created:    
            bh = self.broker_host or '1/1/rabbit_managment_console'
            self.connection.broker_host = bh.split('/')[2]
            self.connection.save()
        # Gateway        
        self.gateway, g_created = Gateway.objects.get_or_create(name=self.gateway_name,connection=self.connection)        
        # Node
        if not (self.node_id ==255): # and self.child_sensor_id ==255):
            self.node, n_created = Node.objects.get_or_create(public_id=self.node_id,eui=self.node_eui,gateway=self.gateway,defaults={'status': 'New'})   
            # Does not save time on heartbeat
            if self.direction == 'out':
                self.node.send_status = timezone.now()
                self.node.save()

        if self.is_gataway_message is False and self.node_id != 255:            
            # Sensor        
            self.sensor, s_created = Sensor.objects.get_or_create(node=self.node,child_sensor_id=self.child_sensor_id)
            # Set default name if 
            if s_created:
                sensor_name = 'NoPresentation-' + str(self.node) + '-' + str(self.child_sensor_id)
                self.sensor.name=sensor_name
                self.sensor.save()


    def process(self):
        """
        Commands:
        presentation	0	Sent by a node when they present attached sensors.
        set	            1	This message is sent from or to a sensor when a sensor value should be updated
        req	            2	Requests a variable value (usually from an actuator destined for controller).
        internal	    3	This is a special internal message. See IternamType class
        stream	        4	Used for OTA firmware updates        
        """
        # First of all - define device ID's
        try:
            self.save_devices()        
            if self.command == 0:
                self.do_presentation()
            elif self.command == 1:
                self.do_set()            
            elif self.command == 2:
                self.do_req()
            elif self.command == 3:
                self.do_internal()            
            elif self.command == 4:
                self.do_stream()                
        except Exception as e:            
            self.log.error(self.mac + '-' + "/".join(self.full_message) + ':' + self.payload)
            self.log.error(e, exc_info=True)
         

