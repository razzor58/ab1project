"""Module t owork with db"""
import psycopg2
# import logging
# from  configparser import ConfigParser
# Define helper objects


class Helper():
    """Perform db opearations"""
    def __init__(self):
        # Read params from config
        # self.log = logging.getLogger('web')
        # config = ConfigParser()
        # config.read('config.ini')
        dbname = "fox"
        user = "postgres"
        password = "psql@pwd123"
        # Connect to an existing database
        conect_parameters = "dbname={0} user={1} password={2}".format(dbname, user, password)
        self.conn = psycopg2.connect(conect_parameters)
        # self.log.info("connect to db")


    def save(self, data):
        """ Open a cursor to perform database operations"""
        sql = """INSERT INTO console_mqttmessage
            (timestamp, consumer_tag, delivery_tag, exchange, redelivered, uri, cluster_name,
            routing_key,payload, gateway, direction,node_id, child_sensor_id, command,ack, type,
            qos,dup)
            VALUES 
            (current_timestamp, '{}', '{}', '{}', '{}', '{}', '{}','{}', '{}', '{}', '{}', '{}',
             '{}', '{}', '{}', '{}', '{}', '{}')""".format(*data)
                            
        try:
            with self.conn.cursor() as cur:
                cur.execute(sql)
                self.conn.commit()
        except Exception as e:
            print(sql)            
            print(e)            
            cur.close()
        finally:
            cur.close()

    def __del__(self):
        self.conn.close()
