#!/usr/local/fox/env/bin/python

import pika
import os
import time
import logging
from configparser import ConfigParser
import db
from sys import platform
import os.path
# Define helper objects
logger = logging.getLogger('rabbit_saver')
logger.setLevel(logging.DEBUG)
# create file handler which logs even debug messages
fh = logging.FileHandler(os.path.join(os.path.abspath(os.path.dirname(__file__)),'rabbit_saver.log'))
fh.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
# add the handlers to the logger
logger.addHandler(fh)
logger.addHandler(ch)
config = ConfigParser()
# config.read('config.ini')
database = db.Helper()

# Read params from config
# url = config['rabbitmq']['url']
# exchange = config['rabbitmq']['exchange']
# routing_key = config['rabbitmq']['routing_key']
url="amqp://webuserbackend:webuserbackend321@hohobo.com:5672/%2f"
exchange="amq.topic"
routing_key="#"

# Create rabbitmq connection
connection = pika.BlockingConnection(pika.URLParameters(url))

# Create rabbit channel
channel = connection.channel()

# Generate rabbitmq queue name
q_name = 'backend_logger_' + platform
queue_name = channel.queue_declare(exclusive=True,queue=q_name).method.queue
# Subsribe on queue messages
channel.queue_bind(exchange=exchange,
                   queue=queue_name,
                   routing_key=routing_key
                  )

# what to do when message reveive from rabbit
def callback(ch, method, properties, body):
    """
        What to do when message arrived
        params:
        consumer_callback(channel, method, properties, body)
        channel: BlockingChannel
        method: spec.Basic.Deliver
        properties: spec.BasicProperties
        body: str or unicode
        consumer_tag, delivery_tag, exchange, redelivered, uri, cluster_name,
            routing_key,payload, gateway, direction, data1, data2, data3, data4,data5
    """
    # print(" [x] %r" % body)
    # print(" [x] %r" % method)
    # print(" [x] %r" % properties)
    # print(" [x] %r" % ch)
    try:
        
        receive_from = properties.headers.get('x-received-from') or [{}]
        h = properties.headers or {}
        rk = 'test-111111-in.0.0.0.0.0' if method.routing_key == q_name else method.routing_key
        # if (rk.split('-')[2]).split('.')[0] =='in':
        #     logger.info(str(ch) + ';' + str(method) + ';' + str(properties) + ';' + str(body))

        data = [
            method.consumer_tag
            , method.delivery_tag
            , method.exchange
            , method.redelivered
            , receive_from[0].get('uri','amqp://backend.hohobo.com')
            , receive_from[0].get('cluster-name','rabbit@mqtt1-gateway')
            , rk
            , body.decode('utf-8')
            , rk.split('-')[1]
            , (rk.split('-')[2]).split('.')[0]
            , rk.split('.')[1]
            , rk.split('.')[2]
            , rk.split('.')[3]
            , rk.split('.')[4]
            , rk.split('.')[5]
            , h.get('x-mqtt-publish-qos') or 0
            , h.get('x-mqtt-dup') or False
            ]
        database.save(data)
    except Exception as e:
        logger.error(e)
        logger.error("problem method:{0} properties: {1}".format(method,properties))
# Define confumer object
channel.basic_consume(callback,
                      queue=queue_name,
                      no_ack=True)
# Start listener
# import daemon
logger.info('start')
channel.start_consuming()
