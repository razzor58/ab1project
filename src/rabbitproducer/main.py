#!/usr/local/fox/env/bin/python
from producer import Producer
import db
from time import sleep



while True:   
    p = Producer()
    database = db.Helper()           

    if database.time_to_node_check:
        first_dataset = database.get_nodes_messages()
        for i in first_dataset:
            try:
                p.publish(i[0], i[1])
            except Exception as e:
                n = Producer()
                n.publish(i[0], i[1])

    if database.time_to_gateway_check:
        second_dataset = database.get_gateways_messages()
        for i in second_dataset:
            try:
                p.publish(i[0], i[1])
            except Exception as e:
                n = Producer()
                n.publish(i[0], i[1])





    sleep(60)
