"""Module t owork with db"""
import django
import sys
import os
import datetime
from django.utils import timezone
# For use django app 
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)),'..'))
os.environ['DJANGO_SETTINGS_MODULE'] = 'web.settings'
django.setup()
from console import models

class Helper():
    """Perform db opearations"""
    def __init__(self):
        self.time_to_gateway_check = False
        self.time_to_node_check = False

        try:  
            v = int(models.AppSettings.objects.get(name='nodes_check_interval').value)
            nodes_check_interval = 29*60 if v ==0 else v*60            
        except Exception:
            nodes_check_interval = 29*60

        try:                    
            g = int(models.AppSettings.objects.get(name='gateways_check_interval').value)
            gateways_check_interval = 45*60 if g ==0 else g*60
        except Exception:
            gateways_check_interval = 45*60
            
            
        gate_timer = models.HeartbeatTimer.objects.get(object_type='gateway')
        node_timer = models.HeartbeatTimer.objects.get(object_type='node')

        moment = timezone.now()
        if (moment - gate_timer.last_check_date).total_seconds() >= gateways_check_interval:
            self.time_to_gateway_check = True
            gate_timer.last_check_date = moment
            gate_timer.save()

        if (moment - node_timer.last_check_date).total_seconds() >= nodes_check_interval:
            self.time_to_node_check = True
            node_timer.last_check_date = moment
            node_timer.save()


        
    def get_nodes_messages(self):
        """
        gateway-DEADBE058E11-in/0/255/3/0/18 раз в X

        """
        messages = []
        for i in models.Node.objects.all():
            name = i.eui.replace('out','in').replace('/','.')
            messages.append((name + '.255.3.0.18',''))
        return messages
    
    def get_gateways_messages(self):
        """
        gateway-DEADBE058E11-in/255/255/3/0/20 раз в Y
        """    
        messages = []
        for i in models.Gateway.objects.all():
            name = i.name + '-in.255.'
            messages.append((name + '255.3.0.20',''))
        return messages
