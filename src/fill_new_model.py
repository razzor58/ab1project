
import sys
import os
import django
# For use django app
sys.path.append(os.path.join(os.path.dirname(os.path.abspath(__file__)),'..'))
os.environ['DJANGO_SETTINGS_MODULE'] = 'web.settings'
django.setup()

import rabbitparser.mqtt_types as t
import console.models as m

for i in range(28):
    o = t.InternalTypes(i)
    new_item = m.ApiMessageTypeCommand(mqtt_type=o.name,value=i,comment='Send  ' + o.name.replace('I_','') + ' command')
    new_item.save()
