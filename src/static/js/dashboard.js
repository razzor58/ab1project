$(function () {


var init_divs = function(node_eui) {
        $.post('/console/get_nodes_sensors',{'node_eui':node_eui})
            .done(function(data){
                var row_sum = 0,
                row_content = [],
                // row_start = '<div class="row empty">',
                row_end = '</div>'

                for (var i=0;i<data.length;i++){
                    var row_start = '<div class="row empty ' + data[i] + '">'
                    var content = '<div class="col-lg-6"><div class="ibox float-e-margins"><div class="ibox-title"><h5>' 
                                            + data[i]
                                            + '</h5></div><div class="ibox-content"><div><canvas id="chart_' 
                                            + data[i]
                                            + '" height="206" width="442" style="display: block; width: 442px;'
                                            + ' height: 206px;"></canvas></div></div></div></div>'
                    // row_content.push(content)

                    // if (row_sum%2 == 0){
                        $('#chart_box').append(row_start + content + row_end)
                        // row_content = []
                    // }
                }

             

            })
        }

var init_charts = function(node_eui) {
    $.post('/console/get_chart_data',{'node_eui':node_eui})
        .done(function(data){
            $.each(data,function(ind,chart_data){
                var d = data[ind]
                var ctx = $('#chart_' + chart_data.name);

                var r = $('.' + chart_data.name);
                r.removeClass("empty")

                if (ctx !== 'undefined') {
                    var chart = new Chart(ctx, {        
                        type: 'line',
                        data: {
                            label: chart_data.name,
                            labels: chart_data.time,
                            datasets: [{
                                borderColor: 'rgb(255, 99, 132)',
                                data: chart_data.value,
                                label: chart_data.var_type[0],
                            }]
                        },
                        options: {}
                    });
                }
            });  
            var t = "<span>No data</span>"
            $(".empty").append(t);  
            $(".empty").find('canvas').remove();
    });

}

$.get('/console/get_node_data', function(data){
    $(".typeahead_2").typeahead({ source:data })
},'json'); 


$('#chart_filter').on('click',function(event) {

    $('#chart_box').empty();

   var node  = $('input.typeahead_2').val();
    init_divs(node)
    init_charts(node)
})


    
});