$(function () {
    // 6 create an instance when the DOM is ready
//     $('#jstree').jstree();
//     // 7 bind to events triggered on the tree
//     $('#jstree').on("changed.jstree", function (e, data) {
//       console.log(data.selected);
//     });
//     // 8 interact with the tree - either way is OK
//     $('button').on('click', function () {
//       $('#jstree').jstree(true).select_node('child_node_1');
//       $('#jstree').jstree('select_node', 'child_node_1');
//       $.jstree.reference('#jstree').select_node('child_node_1');
//     });
//   });

var filename = null;


$('#jstree').jstree({
    'core' : {
        'data' : {
            'url' : function(node) {
                return'/console/get_files_data';
            },
            'data' : function(node) {
                return {
                        parentId : node.id ==="#" ? 0 : node.id,
                        searchdepth : 1
                    }
            },
            'success' : function(data) {
                  return data.list
                }
            },
            'check_callback':true,
        },
        "plugins": ["json_data","themes","actions"],
        
    }).on('changed.jstree', function (e, data) {
        var i, j, r = [];
        for(i = 0, j = data.selected.length; i < j; i++) {
          r.push(data.instance.get_node(data.selected[i]).text);
        }

        filename = r.join(', ')
        $("#delete_button").show()
        
      })




      $("#upload_button").on("click", function(){
            $("#form_upload").submit();

            
      });


    //   $("#delete_button").on("click", function(){

    //     $.ajax({
    //         url: "/console/delete",
    //         method:"POST",
    //         data: {"filename":filename}
    //       }).success(function() {
    //             console.log(filename + "was deleted");
    //             $('#jstree').jstree("refresh");
    //       }).fail(function(){

    //         console.log( + "failed")
    //     });
          
    // });
    

    $("#delete_button").click(function(){
        $.post("/console/delete", { "filename":filename },function(data, status){
            console.log(data);
            $('#jstree').jstree("refresh");
        });
    });
//end script
});