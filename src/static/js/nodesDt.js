var node_columnDefs = [
    {
        "targets": 0,
        "data": "gw",
        "orderable": true,
        "render": function (data, type, full) {
            var g = full
            var template = '<div class="gws_table">'
            + '<b>' + g.name + '</b>'
            + '<br>eui: ' +  g.eui_short + ' peui: ' +  g.parent
            + '<br><a href="#" onclick="go_to_sensor_table(event)" data-gate="' + g.gw + '" data-node="' + g.eui + '">Sensors: </a>' + g.sensors + '</div>'
            return template;
        }
    },
    {
        "targets": 1,
        "data": "gw",
        "orderable": true,
        "render": function (data, type, full) {
            var g = full
            var template = '<div class="gws_table"><b>' 
            + g.status + '</b><br>' 
            +  g.gw + '<br>' 
            +  g.client_email + '</div>'
            return template;
        }
    },
    {
        "targets": 2,
        "data": "gw",
        "orderable": true,
        "render": function (data, type, full) {
            var g = full
            var template = '<div class="gws_table"><b>' 
            + g.node_type + '</b><br>' 
            +  g.rssi + ' dB<br>' 
            +  g.bat_level + '% </div>'
            return template
                                                                            
        }
    },
    {
        "targets": 3,
        "data": "gw",
        "orderable": true,
        "render": function (data, type, full) {
            var g = full
            var template = '<div class="gws_table"><b>Version</b><br>' 
            + g.library + '/' + g.firmware_min + '<br>' 
            +  'FW ->' + g.firmware_min + '</div>'
            return template;
        }
    },
    {
        "targets": 4,
        "data": "gw",
        "orderable": true,
        "render": function (data, type, full) {
            var g = full
            var template = '<div class="gws_table"><b>Time</b><br>' 
            + g.time + '<br>' 
            +  g.status_seen_ago +'</div>'
            return template;
        }
    },
    {
        "targets": 5,
        "data": "gw",
        "orderable": true,
        "render": function (data, type, full) {
            var template = '<a href="#" id="gws1" class="gate" '
            + 'data-name="' + full.name +'"'
            + 'data-eui="' + full.eui +'"'
            + 'data-eui_short="' + full.eui_short +'"'
            + 'data-parent="' + full.parent +'"'
            + 'data-node_type="' + full.node_type +'"'
            + 'data-firmware_min="' + full.firmware_min + '"'
            + 'role="button" data-toggle="modal" data-target="#node_edit" data-load-url="/console/gateway_modal?pk=1"><i class="fa fa-edit" title="Edit Node"></i></a>'
            + '| <a href="#" id="gws1" class="gate" role="button" data-toggle="modal" data-target="#node_delete"' 
            + 'data-eui="' + full.eui + '"data-load-url="/console/gateway_modal?pk=1"><i class="fa fa-remove" title="Send command"></i></a>'
            return template;
        }
    },
];

var node_buttons = [

    {
        text: 'назад',
        action: function ( e, dt, node, config ) { 

            dt.destroy()
            $('#NodeTable').empty()
            back_to_gateway();
        }
    }];