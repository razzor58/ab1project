$(function () {

    $('#gateway_modal').on('show.bs.modal', function (e) {
        var loadurl = $(e.relatedTarget).data('load-url');
        $(this).find('.modal-body').load(loadurl);
    });


    $('.gateway-select').select2({
        dropdownParent: $('#addGateway')
    });
     

    $("#addGateway").on('show.bs.modal',function(e){
        var c_id = $(e.relatedTarget).data('client');        
        $("#assignGateway").attr('data-client',c_id);
    });


    $("#assignGateway").on('click',function(e){
        e.preventDefault();

        var c_id = $("#assignGateway").attr('data-client');
        var g_id = $(".gateway-select").val();        
        $.post('/console/assign_gateway',{"g_id":g_id, "c_id":c_id})
            .done(function(data){
                var g = $("#gws" + c_id)
                $("#gws" + c_id).text(data.gws);
                $('#nodes' +  c_id).text(data.nodes);
                $('#sensors' + c_id).text(data.sensors);                
            });

        $("#addGateway").modal('hide');

    });
});