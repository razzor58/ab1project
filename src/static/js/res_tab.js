var show_resources = function(){

    // var context = {
    //     gate: null,
    //     node: null
    // }
    
    
    
    // var extensions = {
    //     "sFilter": "form-control input-sm",
    //     "sLength": "form-control input-sm"
    // }
    // // Used when bJQueryUI is false
    // $.extend($.fn.dataTableExt.oStdClasses, extensions);
    // // Used when bJQueryUI is true
    // $.extend($.fn.dataTableExt.oJUIClasses, extensions);
    
    
    // ===================SENSOR ===============================
    var sensor_table_settings = Object.assign({}, dataTableSettings)
        sensor_table_settings.columnDefs = sensor_columnDefs
        sensor_table_settings.dom='Bfrtip'
        sensor_table_settings.buttons = [
        //     {
        //     text: 'Назад',
        //     action: function ( e, dt, node, config ) { 
    
        //         $('.nav-tabs a[href="#nodes"]').tab('show')
        //         // dt.destroy()
        //         // $('#SensorTable').empty()
        //         // $('#NodeTable').DataTable(node_table_settings);
        //     }
        // },
        { text: 'Обновить',
        action: function ( e, dt, node, config ) { 
            dt.ajax.reload()
        }
    }]
    
    // =================== NODE ===============================
    var node_table_settings = Object.assign({}, dataTableSettings)
        node_table_settings.dom='Bfrtip'
        node_table_settings.columnDefs = node_columnDefs
        node_table_settings.buttons = [
            // {
            // text: 'Назад',
            // action: function ( e, dt, node, config ) { 
            //     $('.nav-tabs a[href="#gateways"]').tab('show')
            //     // dt.destroy()
            //     // $('#NodeTable').empty()
            //     // $('#GatewayTable').DataTable(gateway_table_settings);
            // },
        //},
        { text: 'Обновить',
        action: function ( e, dt, node, config ) { 
            dt.ajax.reload()
        }
    }
    ]
    // ===========GATE ==================================
    var gateway_table_settings = Object.assign({}, dataTableSettings)
        gateway_table_settings.ajax =  {
                    'url': '/console/get_gateway_table/', 'type': 'POST',
        }
        gateway_table_settings.columnDefs =  gate_columnDefs
        node_table_settings.dom='Bfrtip'
        gateway_table_settings.buttons = [
        { text: 'Обновить',
        action: function ( e, dt, node, config ) { 
            dt.ajax.reload()
        }
    }
    ]
    
    // ===============================================================
    
    // var go_to_node_table = function(event) {
        
    //     context.gate = $(event.currentTarget).attr('data-gate')
    //     $('.nav-tabs a[href="#nodes"]').tab('show')
    
    // }
    // ===============================================================
    
    // var go_to_sensor_table = function(event) {
        
    //     context.node = $(event.currentTarget).attr('data-node');
    //     context.gate = $(event.currentTarget).attr('data-gate');
    //     $('.nav-tabs a[href="#sensors"]').tab('show')
    
    // }
    
    var el  = $('#GatewayTable')
    var gate_table = el.DataTable(gateway_table_settings);
    $('div.dataTables_filter input').addClass('form-control');
    $('div.dataTables_filter input').addClass('input-xs');
    
    
    
    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        var target = $(e.target).attr("href");
        if (target == '#nodes') {
    
            if ( ! $.fn.DataTable.isDataTable('#NodeTable' ) ) {
                var s = node_table_settings
                s.ajax = {
                    'url': '/console/get_node_table/', 'type': 'POST',
                    'data': function (d) {
                        d.gate_name = context.gate
                    }
                }
                $('#NodeTable').DataTable(node_table_settings);
              }
            else {
                $('#NodeTable').DataTable().ajax.reload();
            }
        } 
        else if (target == '#sensors') {
            
            if ( ! $.fn.DataTable.isDataTable('#SensorTable') ) {
                sensor_table_settings.ajax = { 'url': '/console/get_sensor_table/', 'type': 'POST',
                    'data': function (d) {
                        d.node_name = context.node
                        d.gate_name = context.gate
                    }
                }
                $('#SensorTable').DataTable(sensor_table_settings);
              }
            else {
                $('#SensorTable').DataTable().ajax.reload();
            }
            
        }
        else {
    
            if ( ! $.fn.DataTable.isDataTable('#GatewayTable' ) ) {
                gateway_table_settings.ajax = {
                    'url': '/console/get_gateway_table/', 'type': 'POST',
                }
                $('#GatewayTable').DataTable(gateway_table_settings);
              }
            else {
                $('#GatewayTable').DataTable().ajax.reload();
            }
        }
    
        context = {
            gate: null,
            node: null
        }
        
    
    
    
        
    });
    

}


var context = {
    gate: null,
    node: null
}

var go_to_node_table = function(event) {
        
    context.gate = $(event.currentTarget).attr('data-gate')
    $('.nav-tabs a[href="#nodes"]').tab('show')

}

var go_to_sensor_table = function(event) {
        
    context.node = $(event.currentTarget).attr('data-node');
    context.gate = $(event.currentTarget).attr('data-gate');
    $('.nav-tabs a[href="#sensors"]').tab('show')

}

    

