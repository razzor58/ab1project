var gate_columnDefs =  [
    
    {  "targets": 0,
        "data": "name",
        "orderable": true,
        "render": function (data, type, full) {
            var g = full
            var template = '<div class="gws_table"><b>' 
            + g.node_uid + '</b><br>uid:' 
            +  g.name + '<br><a href="#" onclick="go_to_node_table(event)" data-gate="' + g.name +'">Nodes: </a>' 
            +  g.nodes + '<a href="#" onclick="go_to_sensor_table(event)" data-gate="' + g.name +'">&nbspSensors: </a>' 
            +  g.sensors   + '</div>'
            return template;
        }
    },
    { "targets": 1,
        "data": "name",
        "orderable": true,
        "render": function (data, type, full) {
            var g = full
            var template = '<div class="gws_table"><b>' 
            + g.status + '</b><br>' 
            +  g.broker_host + '<br>' 
            +  g.client_email + '</div>'
            return template;
        }
    },
    { "targets": 2,
        "data": "name",
        "orderable": true,
        "render": function (data, type, full) {
            var g = full
            var template = '<div class="gws_table"><b>' 
            + g.node_type + '</b><br>' 
            +  g.rssi + ' dB<br>' 
            +  g.bat_level + '% </div>'
            return template
                                                                            
        }
    },
    { "targets": 3,
        "data": "name",
        "orderable": true,
        "render": function (data, type, full) {
            var g = full
            var template = '<div class="gws_table"><b>Version</b><br>' 
            + g.library + '/' + g.firmware_min + '<br>' 
            +  'FW ->' + g.firmware_min + '</div>'
            return template;
        }
    },
    { "targets": 4,
        "data": "name",
        "orderable": true,
        "render": function (data, type, full) {
            var g = full
            var template = '<div class="gws_table"><b>Time</b><br>' 
            + g.send_status_ago + '<br>' 
            +  g.status_seen_ago +'</div>'
            return template;
        }
    },
    {
        "targets": 5,
        "data": "gw",
        "orderable": true,
        "render": function (data, type, full) {
            var template = '<a href="#" id="gws1" class="gate" '
            + 'data-name="' + full.name +'"'
            + 'data-eui="' + full.eui +'"'
            + 'data-eui_short="' + full.eui_short +'"'
            + 'data-parent="' + full.parent +'"'
            + 'data-node_type="' + full.node_type +'"'
            + 'data-firmware_min="' + full.firmware_min + '"'
            + 'role="button" data-toggle="modal" data-target="#gate_edit" data-load-url="/console/gateway_modal?pk=1"><i class="fa fa-edit" title="Edit Node"></i></a>'
            + '| <a href="#" id="gws1" class="gate" role="button" data-toggle="modal" data-target="#gate_remove"' 
            + 'data-eui="' + full.eui + '"data-load-url="/console/gateway_modal?pk=1"><i class="fa fa-remove" title="Send command"></i></a>'
            return template;
        }
    },
]