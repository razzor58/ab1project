
var go_to_node_table = function(event) {
    
    var gate_name = $(event.currentTarget).attr('data-gate');
    var node_table_settings = Object.assign({}, dataTableSettings)

    node_table_settings.dom='Bfrtip'
    node_table_settings.ajax = {
        'url': '/console/get_node_table/', 'type': 'POST',
        'data': function (d) {
            d.gate_name = gate_name
        }
    }

    node_table_settings.columnDefs = node_columnDefs
    node_table_settings.buttons= [
        {
            text: 'назад',
            action: function ( e, dt, node, config ) { 
                dt.destroy()
                $('#NodeTable').empty()
                
                $('#GatewayTable').DataTable(gateway_table_settings);
                // gate_table.draw()
            }
        }
    ]
    

    var t = $('#GatewayTable').DataTable();
    t.destroy();

    $('#GatewayTable').empty();
    var t1 = $('#NodeTable').DataTable(node_table_settings);


}


var go_to_sensor_table = function(event) {
    
    var node_name = $(event.currentTarget).attr('data-node');
    var sensor_table_settings = Object.assign({}, dataTableSettings)
    sensor_table_settings.ajax = {
        'url': '/console/get_sensor_table/', 'type': 'POST',
        'data': function (d) {
            d.node_name = node_name
        }
    }

    sensor_table_settings.columnDefs = sensor_columnDefs

    var t = $('#NodeTable').DataTable();
    t.destroy();

    $('#NodeTable').empty();
    var t1 = $('#SensorTable').DataTable(sensor_table_settings);


}

var gateway_table_settings = Object.assign({}, dataTableSettings)

gateway_table_settings.ajax =  {
            'url': '/console/get_gateway_table/', 'type': 'POST',
            'data': function (d) {
                d.date_start = 'date_start';
                d.date_end = 'date_end';
            }
}
gateway_table_settings.columnDefs =  gate_columnDefs

var gate_table = $('#GatewayTable').DataTable(gateway_table_settings);

 
var draw_gateway_table = function() {
    gate_table.draw()
}