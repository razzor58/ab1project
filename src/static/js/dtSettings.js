var dataTableSettings = {
    serverSide: false,
    paging: true,
    pagingType: "numbers",
    searching: true,
    info: true,
    // bLengthChange: true,
    lengthMenu: [ 3, 5, 10, 20, 50 ],
    // iDisplayLength: 5,
    scrollX: true,
    fixedColumns: true,
    autoWidth: false,
    pageLength: 2,
    dom: 'Blfrtip',
    initComplete: function( settings, json ) {
        var a = $(this).closest('.dataTables_length')
        $(this).closest('.dataTables_length').find('> select').addClass('form-control')

      $('#GatewayTable tbody').on('dblclick', 'tr', function () {
          var a = $(this).attr('data-name');
          $('#gate_modal').attr('data-name',$(this).attr('data-name'));
          $('#gate_modal').attr('data-eui',$(this).attr('data-eui'));
          $('#gate_modal').attr('data-firmware_min',$(this).attr('data-firmware_min'));
          $('#gate_modal').attr('data-parent',$(this).attr('data-parent'));
          $('#gate_modal').attr('data-node_type',$(this).attr('data-node_type'));
          $('#gate_modal').attr('data-client_email',$(this).attr('data-client_email'));
          $('#gate_modal').attr('data-send_status_ago',$(this).attr('data-send_status_ago'));


          $('#gate_modal').modal('toggle');
      });

        
    },
    language: {
      "processing": "Подождите...",
      "search": "",
      "searchPlaceholder": "Поиск",
      "lengthMenu": "Показать _MENU_ записей",
      "info": "Записи с _START_ до _END_ из _TOTAL_ записей",
      "infoEmpty": "Записи с 0 до 0 из 0 записей",
      "infoFiltered": "(отфильтровано из _MAX_ записей)",
      "infoPostFix": "",
      "loadingRecords": "Загрузка записей...",
      "zeroRecords": "Записи отсутствуют.",
      "emptyTable": "В таблице отсутствуют данные",
      "paginate": {
        "first": "Первая",
        "previous": "Предыдущая",
        "next": "Следующая",
        "last": "Последняя"
      },
      "aria": {
        "sortAscending": ": активировать для сортировки столбца по возрастанию",
        "sortDescending": ": активировать для сортировки столбца по убыванию"
      }
    }
  };