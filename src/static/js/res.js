
var context = {
    gate: null,
    node: null
}

var context_for_reload = {
    gate: null,
    node: null
}


// var extensions = {
//     "sFilter": "form-control input-sm",
//     "sLength": "form-control input-sm"
// }
// // Used when bJQueryUI is false
// $.extend($.fn.dataTableExt.oStdClasses, extensions);
// // Used when bJQueryUI is true
// $.extend($.fn.dataTableExt.oJUIClasses, extensions);


// ===================SENSOR ===============================
var sensor_table_settings = Object.assign({}, dataTableSettings)
    sensor_table_settings.columnDefs = sensor_columnDefs
    sensor_table_settings.dom='Blfrtip'
    sensor_table_settings.buttons = [
    //     {
    //     text: 'Назад',
    //     action: function ( e, dt, node, config ) { 

    //         $('.nav-tabs a[href="#nodes"]').tab('show')
    //         // dt.destroy()
    //         // $('#SensorTable').empty()
    //         // $('#NodeTable').DataTable(node_table_settings);
    //     }
    // },
    { text: 'Обновить',
    action: function ( e, dt, node, config ) { 
        console.log(context)
        dt.ajax.reload()
    }
}]

// =================== NODE ===============================
var node_table_settings = Object.assign({}, dataTableSettings)
    node_table_settings.dom='Blfrtip'
    node_table_settings.columnDefs = node_columnDefs
    node_table_settings.createdRow = function( row, data, dataIndex ) {

        $(row).attr('data-name', data.name);
        $(row).attr('data-firmware_min', data.firmware_min);
        $(row).attr('data-parent', data.parent);
        $(row).attr('data-node_type', data.node_type);
        $(row).attr('data-eui', data.eui);
        $(row).attr('data-eui_short', data.eui_short);
        
    }
    node_table_settings.buttons = [
        // {
        // text: 'Назад',
        // action: function ( e, dt, node, config ) { 
        //     $('.nav-tabs a[href="#gateways"]').tab('show')
        //     // dt.destroy()
        //     // $('#NodeTable').empty()
        //     // $('#GatewayTable').DataTable(gateway_table_settings);
        // },
    //},
    { text: 'Обновить',
    action: function ( e, dt, node, config ) { 
        dt.ajax.reload()
    }
}
]
// ===========GATE ==================================
var gateway_table_settings = Object.assign({}, dataTableSettings)
    gateway_table_settings.ajax =  {
                'url': '/console/get_gateway_table/', 'type': 'POST',
    }
    gateway_table_settings.columnDefs =  gate_columnDefs
    node_table_settings.dom='Blfrtip'
    gateway_table_settings.createdRow = function( row, data, dataIndex ) {

        $(row).attr('data-name', data.node_uid);
        $(row).attr('data-firmware_min', data.firmware_min);
        // $(row).attr('data-parent', data.parent);
        $(row).attr('data-node_type', data.node_type);
        $(row).attr('data-eui', data.name);
        $(row).attr('data-eui_short', data.eui_short);
        $(row).attr('data-client_email', data.client_email);
        $(row).attr('data-send_status_ago', data.send_status_ago);
        
    }
    gateway_table_settings.buttons = [
    { text: 'Обновить',
    action: function ( e, dt, node, config ) { 
        dt.ajax.reload()
    }
}
]

// ===============================================================

var go_to_node_table = function(event) {
    
    context.gate = $(event.currentTarget).attr('data-gate')
    $('.nav-tabs a[href="#nodes"]').tab('show')

}
// ===============================================================

var go_to_sensor_table = function(event) {
    
    context.node = $(event.currentTarget).attr('data-node');
    context.gate = $(event.currentTarget).attr('data-gate');
    // var d = $(event.currentTarget).attr('data-gate')

    // var gate_table = $('#GatewayTable').DataTable(gateway_table_settings);
    // oTable.fnFilter(d);

    // context_for_reload.node = $(event.currentTarget).attr('data-node');
    // context_for_reload.gate = $(event.currentTarget).attr('data-gate');

    $('.nav-tabs a[href="#sensors"]').tab('show')

}


var gate_table = $('#GatewayTable').DataTable(gateway_table_settings);
$('div.dataTables_filter input').addClass('form-control');
$('div.dataTables_filter input').css('margin-left','1px');
$('div.dataTables_filter input').css('text-align','start');




$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
    var target = $(e.target).attr("href");
    if (target == '#nodes') {

        if ( ! $.fn.DataTable.isDataTable('#NodeTable' ) ) {
            var s = node_table_settings
            s.ajax = {
                'url': '/console/get_node_table/', 'type': 'POST',
                'data': function (d) {
                    d.gate_name = context.gate
                }
            }
            $('#NodeTable').DataTable(node_table_settings);
            $('div.dataTables_filter input').addClass('form-control');
            $('div.dataTables_filter input').css('margin-left','1px');
            $('div.dataTables_filter input').css('text-align','start');
          }
        else {
            $('#NodeTable').DataTable().ajax.reload();
        }
    } 
    else if (target == '#sensors') {
        
        if ( ! $.fn.DataTable.isDataTable('#SensorTable') ) {
            sensor_table_settings.ajax = { 'url': '/console/get_sensor_table/', 'type': 'POST',
                'data': function (d) {
                    d.node_name = context.node
                    d.gate_name = context.gate
                }
            }
            $('#SensorTable').DataTable(sensor_table_settings);

            $('div.dataTables_filter input').addClass('form-control');
            $('div.dataTables_filter input').css('margin-left','1px');
            $('div.dataTables_filter input').css('text-align','start');
            $.fn.dataTable.ext.classes.sLengthSelect = 'form-control';
          }
        else {
            $('#SensorTable').DataTable().ajax.reload();
        }
        
    }
    else {

        if ( ! $.fn.DataTable.isDataTable('#GatewayTable' ) ) {
            gateway_table_settings.ajax = {
                'url': '/console/get_gateway_table/', 'type': 'POST',
            }
            $('#GatewayTable').DataTable(gateway_table_settings);
          }
        else {
            $('#GatewayTable').DataTable().ajax.reload();
        }
    }

 
    context = {
        gate: null,
        node: null
    }



    
});

$('#gate_modal').on('shown.bs.modal',function(event){
    $('#node_short_info').remove()
    $("#send_result").empty()
    var b = $(this)

    $("#send_command").attr('data-eui',b.attr('data-eui'))

    var node_name = b.attr('data-name');
    var firmware_min = b.attr('data-firmware_min');
    var parent = b.attr('data-parent');
    var eui = b.attr('data-eui');
    var node_type = b.attr('data-node_type');
    var user = b.attr('data-client_email');
    var send_status_ago = b.attr('data-send_status_ago');
    
    
    var tbl = '<div class="table-responsive" id="node_short_info"><table class="table table-striped"><tbody>'  
             +      '<tr><td>node_uid: </td>'
             +      '<td>' + node_name + '</td></tr>'                                       
              +      '<tr><td>name: </td>'
              +      '<td>' + eui + '</td></tr>'    
              +      '<tr><td>working time: </td>'
              +      '<td>' + send_status_ago + '</td></tr>'                
              +      '<tr><td>Node Type: </td>'
              +      '<td>' + node_type + '</td></tr>' 
              +      '<tr><td>firmware_min: </td>'
              +      '<td>' + firmware_min  + '</td></tr>' 
              +      '<tr><td>user: </td>'
              +      '<td>' + user  + '</td></tr>' 

              + '</tbody></table></div>'  

    $('#node_info').append(tbl)
})




$('#node_edit').on('shown.bs.modal',function(event){

    $('#node_short_info').remove()
    $("#send_result").empty()
    var b = $(event.relatedTarget)
    $("#send_command").attr('data-eui',b.attr('data-eui'))

    var firmware_min = '<input type="text" name="firmware_min" value="' + b.attr('data-firmware_min') + '" class="form-control">'
    var parent = '<input type="text" name="parent" value="' + b.attr('data-parent') + '" class="form-control">'
    var eui = '<input type="text" name="eui" value="' + b.attr('data-eui') + '" class="form-control">'
    var node_type = '<input type="text" name="node_type" value="' + b.attr('data-node_type') + '" class="form-control">'

    // var b = event.currentTarget
    var tbl = '<div class="table-responsive" id="node_short_info"><table class="table table-striped"><tbody>'                                  
              +      '<tr><td>EUI: </td>'
              +      '<td>' + eui + '</td></tr>'                
              +      '<tr><td>Parent: </td>'
              +      '<td>' + parent + '</td></tr>' 
              +      '<tr><td>Node Type: </td>'
              +      '<td>' + node_type + '</td></tr>' 
              +      '<tr><td>firmware_min: </td>'
              +      '<td>' + firmware_min  + '</td></tr>' 
              + '</tbody></table></div>'  

    $('#node_edit_info').append(tbl)
})




$('#node_delete').on('shown.bs.modal',function(event){
    var v = $(event.relatedTarget).attr("data-eui")
   $("#delete_node_button").attr("data-eui",v)
})


$('#delete_node_button').on('click',function(event){
    var elem = $(this).attr('data-eui')
    var post_params = {
        'eui': elem
    }
    

    $.post("/console/delete_node", post_params).done(function(response){
        
        $('#node_delete').modal('hide');
        $('#NodeTable').DataTable().ajax.reload();


    });

})


$('#save_node_edit').on('click',function(event){
    var post_params = {}
    var elems = $('#node_short_info').find('input');
    $.each(elems, function(index, item) {
        post_params[$(item).attr('name')] = item.value
    })

    $.post("/console/edit_node", post_params).done(function(response){
        $("#send_result").append(response.result)

        $('#node_edit').modal('hide');
        $('#NodeTable').DataTable().ajax.reload();
        
    });


})


$('#send_command').on('click',function(event){

    var post_params = {
         "device": $(event.currentTarget).attr('data-eui'),
        "message": $("#node_commnand_list").val()
    };

    $.post("/console/send_mqtt", post_params).done(function(response){
        $("#send_result").append(response.result)
    });


});
