var sensor_columnDefs =  [
    
    {  "targets": 0,
        "data": "name",
        "orderable": true,
        "render": function (data, type, full) {
            var g = full
            var template = '<div class="gws_table"><b>' 
            + g.name + '</b><br>' 
            +  'sid: '  + g.child_sensor_id +' eui: ' + g.eui_s + '<br></div>'
            return template;
        }
    },
    { "targets": 1,
        "data": "name",
        "orderable": true,
        "render": function (data, type, full) {
            var g = full
            var template = '<div class="gws_table">' 
            + g.node_name  + '<br>' 
            +  g.eui + '<div>' 
            return template;
        }

    },
    { "targets": 2,
        "data": "name",
        "orderable": true,
        "render": function (data, type, full) {
            var g = full
            var template = '<div class="gws_table">' + g.node_type + '</div>'
            return template
                                                                            
        }
    },
    { "targets": 3,
        "data": "name",
        "orderable": true,
        "render": function (data, type, full) {
            var g = full
            var template = '<div class="gws_table"><br>';
            var d = full.sens_data
            for (i = 0; i < d.length; i++) { 
                template += d[i].variable_type + "<br>";
            }
            return template;
        }
    },
    { "targets": 4,
        "data": "name",
        "orderable": true,
        "render": function (data, type, full) {
            var g = full
            var template = '<div class="gws_table"><b>Value</b><br>';
            var d = full.sens_data
            for (i = 0; i < d.length; i++) { 
                template += d[i].value + "<br>";
            }
            return template;
        }
    },
    { "targets": 5,
    "data": "name",
    "orderable": true,
    "render": function (data, type, full) {
        var g = full
        var template = '<div class="gws_table"><b>Time</b><br>';
        var d = full.sens_data
        for (i = 0; i < d.length; i++) { 
            template += d[i].time + "<br>";
        }
        return template;
    }
},
]