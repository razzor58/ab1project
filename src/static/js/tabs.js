$(function () {


    $('.menu_custom').on('click', function (e) {
        
        $('#tabs_content').empty();

        var route = e.currentTarget.hash.replace("#","")

        $( "#tabs_content" ).load( "/console/get_tab_content?tab=" + route ,function(){
            
            if (route == 'resources_tab') {
                show_resources();
            }
            
        })

    })

});