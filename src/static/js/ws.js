
$(document).ready(function () {
var d = $('.log');
var last_id=0, new_last_id=0;
var get_messages = function(){
        $.getJSON( "get_messages_for_console?max_id="+ last_id, function( data ) {
                for (var i=0; i<data.length; i++) {
                        var message = data[i].fields; 
                        var id= data[i].pk;                       
                        if (id > last_id) {                                
                                // d.after(message.timestamp + " " + prop + " " + message.body +  " <br/>");
                                d.after(message.timestamp.replace('T',' ').split('.')[0]
                                 + " qos: " + "0"                               
                                 + " dup: " + message.redelivered 
                                 + " redelivered: " + message.redelivered
                                 + " exchange: " + message.exchange 
                                 + " cluster-name: " + message.cluster_name 
                                 + " uri: " + message.uri
                                 + (" routing_key: " + message.routing_key).replace(/\./g,'/')
                                 + " payload: " + message.payload
                                 +  " <br/>");
                        }

                        if (id > new_last_id) {                                
                                new_last_id = id;
                        }
                }
                last_id = new_last_id;

                // if ($("#resource_table").length ) {
                //         for (var i=0; i<data.info.length; i++) {
                //                 var d = data.info[i]                                
                //                 $("#" + d.id).text(d.val)
                //        }
                // }
        });
};

setInterval(function(){
                get_messages();
        },2000);
});      