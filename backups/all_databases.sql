--
-- PostgreSQL database cluster dump
--

SET default_transaction_read_only = off;

SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;

--
-- Roles
--

CREATE ROLE davydov;
ALTER ROLE davydov WITH SUPERUSER INHERIT CREATEROLE CREATEDB LOGIN NOREPLICATION NOBYPASSRLS;
CREATE ROLE postgres;
ALTER ROLE postgres WITH SUPERUSER INHERIT CREATEROLE CREATEDB LOGIN REPLICATION BYPASSRLS;






--
-- Database creation
--

CREATE DATABASE davydov WITH TEMPLATE = template0 OWNER = davydov;
CREATE DATABASE fox WITH TEMPLATE = template0 OWNER = davydov;
REVOKE CONNECT,TEMPORARY ON DATABASE template1 FROM PUBLIC;
GRANT CONNECT ON DATABASE template1 TO PUBLIC;


\connect davydov

SET default_transaction_read_only = off;

--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- PostgreSQL database dump complete
--

\connect fox

SET default_transaction_read_only = off;

--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: auth_group; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE auth_group (
    id integer NOT NULL,
    name character varying(80) NOT NULL
);


ALTER TABLE auth_group OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_group_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_group_id_seq OWNER TO postgres;

--
-- Name: auth_group_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_group_id_seq OWNED BY auth_group.id;


--
-- Name: auth_group_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE auth_group_permissions (
    id integer NOT NULL,
    group_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE auth_group_permissions OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_group_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_group_permissions_id_seq OWNER TO postgres;

--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_group_permissions_id_seq OWNED BY auth_group_permissions.id;


--
-- Name: auth_permission; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE auth_permission (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    content_type_id integer NOT NULL,
    codename character varying(100) NOT NULL
);


ALTER TABLE auth_permission OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_permission_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_permission_id_seq OWNER TO postgres;

--
-- Name: auth_permission_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_permission_id_seq OWNED BY auth_permission.id;


--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE auth_user (
    id integer NOT NULL,
    password character varying(128) NOT NULL,
    last_login timestamp with time zone,
    is_superuser boolean NOT NULL,
    username character varying(150) NOT NULL,
    first_name character varying(30) NOT NULL,
    last_name character varying(30) NOT NULL,
    email character varying(254) NOT NULL,
    is_staff boolean NOT NULL,
    is_active boolean NOT NULL,
    date_joined timestamp with time zone NOT NULL
);


ALTER TABLE auth_user OWNER TO postgres;

--
-- Name: auth_user_groups; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE auth_user_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL
);


ALTER TABLE auth_user_groups OWNER TO postgres;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_user_groups_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_groups_id_seq OWNER TO postgres;

--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_user_groups_id_seq OWNED BY auth_user_groups.id;


--
-- Name: auth_user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_id_seq OWNER TO postgres;

--
-- Name: auth_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_user_id_seq OWNED BY auth_user.id;


--
-- Name: auth_user_user_permissions; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE auth_user_user_permissions (
    id integer NOT NULL,
    user_id integer NOT NULL,
    permission_id integer NOT NULL
);


ALTER TABLE auth_user_user_permissions OWNER TO postgres;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auth_user_user_permissions_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE auth_user_user_permissions_id_seq OWNER TO postgres;

--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auth_user_user_permissions_id_seq OWNED BY auth_user_user_permissions.id;


--
-- Name: console_appsettings; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE console_appsettings (
    id integer NOT NULL,
    name character varying(100),
    value character varying(100),
    rus_name character varying(100)
);


ALTER TABLE console_appsettings OWNER TO postgres;

--
-- Name: console_appsettings_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE console_appsettings_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE console_appsettings_id_seq OWNER TO postgres;

--
-- Name: console_appsettings_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE console_appsettings_id_seq OWNED BY console_appsettings.id;


--
-- Name: console_appversion; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE console_appversion (
    id integer NOT NULL,
    "timestamp" timestamp with time zone NOT NULL,
    number character varying(50)
);


ALTER TABLE console_appversion OWNER TO postgres;

--
-- Name: console_appversion_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE console_appversion_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE console_appversion_id_seq OWNER TO postgres;

--
-- Name: console_appversion_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE console_appversion_id_seq OWNED BY console_appversion.id;


--
-- Name: console_city; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE console_city (
    id integer NOT NULL,
    name character varying(100),
    short_name character varying(10),
    rus_name character varying(100),
    timezone character varying(50),
    latitude double precision,
    longitude double precision,
    population integer
);


ALTER TABLE console_city OWNER TO postgres;

--
-- Name: console_city_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE console_city_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE console_city_id_seq OWNER TO postgres;

--
-- Name: console_city_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE console_city_id_seq OWNED BY console_city.id;


--
-- Name: console_client; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE console_client (
    id integer NOT NULL,
    reg_date timestamp with time zone NOT NULL,
    name character varying(100) NOT NULL,
    email character varying(100) NOT NULL,
    phones character varying(100) NOT NULL,
    locale character varying(200) NOT NULL,
    status character varying(50) NOT NULL,
    city_id integer
);


ALTER TABLE console_client OWNER TO postgres;

--
-- Name: console_client_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE console_client_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE console_client_id_seq OWNER TO postgres;

--
-- Name: console_client_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE console_client_id_seq OWNED BY console_client.id;


--
-- Name: console_clientmessage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE console_clientmessage (
    id integer NOT NULL,
    text character varying(200) NOT NULL,
    channel character varying(10) NOT NULL
);


ALTER TABLE console_clientmessage OWNER TO postgres;

--
-- Name: console_clientmessage_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE console_clientmessage_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE console_clientmessage_id_seq OWNER TO postgres;

--
-- Name: console_clientmessage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE console_clientmessage_id_seq OWNED BY console_clientmessage.id;


--
-- Name: console_clientmessagequeue; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE console_clientmessagequeue (
    id integer NOT NULL,
    create_date timestamp with time zone NOT NULL,
    status integer NOT NULL,
    delivery_date timestamp with time zone NOT NULL,
    client_id integer NOT NULL,
    message_id integer NOT NULL
);


ALTER TABLE console_clientmessagequeue OWNER TO postgres;

--
-- Name: console_clientmessagequeue_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE console_clientmessagequeue_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE console_clientmessagequeue_id_seq OWNER TO postgres;

--
-- Name: console_clientmessagequeue_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE console_clientmessagequeue_id_seq OWNED BY console_clientmessagequeue.id;


--
-- Name: console_connection; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE console_connection (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    delay integer NOT NULL,
    network_type character varying(50) NOT NULL,
    qos integer NOT NULL,
    broker_host character varying(50) NOT NULL,
    topic_publish character varying(50) NOT NULL,
    topic_subscribe character varying(50) NOT NULL,
    user_login character varying(50) NOT NULL,
    password character varying(50) NOT NULL,
    client_id_id integer
);


ALTER TABLE console_connection OWNER TO postgres;

--
-- Name: console_connection_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE console_connection_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE console_connection_id_seq OWNER TO postgres;

--
-- Name: console_connection_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE console_connection_id_seq OWNED BY console_connection.id;


--
-- Name: console_firmware; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE console_firmware (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    document character varying(100),
    is_latest integer NOT NULL,
    uploaded_at timestamp with time zone NOT NULL,
    uploaded_by_id integer,
    file_size double precision,
    file_hash character varying(455),
    firmware_type character varying(100),
    file_type integer NOT NULL,
    node_model_id integer
);


ALTER TABLE console_firmware OWNER TO postgres;

--
-- Name: console_firmware_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE console_firmware_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE console_firmware_id_seq OWNER TO postgres;

--
-- Name: console_firmware_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE console_firmware_id_seq OWNED BY console_firmware.id;


--
-- Name: console_firmwarelog; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE console_firmwarelog (
    id integer NOT NULL,
    "timestamp" timestamp with time zone NOT NULL,
    request_meta character varying(500) NOT NULL,
    request_data character varying(500) NOT NULL,
    responce_data character varying(500) NOT NULL,
    node_id integer,
    method character varying(50)
);


ALTER TABLE console_firmwarelog OWNER TO postgres;

--
-- Name: console_firmwarelog_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE console_firmwarelog_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE console_firmwarelog_id_seq OWNER TO postgres;

--
-- Name: console_firmwarelog_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE console_firmwarelog_id_seq OWNED BY console_firmwarelog.id;


--
-- Name: console_gateway; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE console_gateway (
    id integer NOT NULL,
    status_broker character varying(50),
    mobile character varying(50),
    web character varying(50),
    uid_gateway character varying(50),
    status_seen character varying(50),
    seen_message character varying(50),
    nodes_sensors character varying(50),
    topology character varying(50),
    connection_id integer,
    name character varying(80),
    client_id integer,
    status character varying(20),
    inclusion_mode integer NOT NULL,
    reg_date timestamp with time zone
);


ALTER TABLE console_gateway OWNER TO postgres;

--
-- Name: console_gateway_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE console_gateway_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE console_gateway_id_seq OWNER TO postgres;

--
-- Name: console_gateway_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE console_gateway_id_seq OWNED BY console_gateway.id;


--
-- Name: console_heartbeattimer; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE console_heartbeattimer (
    id integer NOT NULL,
    object_type character varying(30),
    last_check_date timestamp with time zone
);


ALTER TABLE console_heartbeattimer OWNER TO postgres;

--
-- Name: console_heartbeattimer_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE console_heartbeattimer_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE console_heartbeattimer_id_seq OWNER TO postgres;

--
-- Name: console_heartbeattimer_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE console_heartbeattimer_id_seq OWNED BY console_heartbeattimer.id;


--
-- Name: console_mqttmessage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE console_mqttmessage (
    id integer NOT NULL,
    "timestamp" timestamp with time zone NOT NULL,
    consumer_tag character varying(200),
    delivery_tag character varying(200),
    exchange character varying(200),
    redelivered boolean NOT NULL,
    uri character varying(200),
    cluster_name character varying(20),
    routing_key character varying(50),
    payload character varying(100),
    gateway character varying(100),
    direction character varying(3) NOT NULL,
    node_id integer NOT NULL,
    command integer NOT NULL,
    ack integer NOT NULL,
    type integer NOT NULL,
    child_sensor_id integer NOT NULL,
    dup boolean NOT NULL,
    qos integer NOT NULL
);


ALTER TABLE console_mqttmessage OWNER TO postgres;

--
-- Name: console_mqttmessage_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE console_mqttmessage_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE console_mqttmessage_id_seq OWNER TO postgres;

--
-- Name: console_mqttmessage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE console_mqttmessage_id_seq OWNED BY console_mqttmessage.id;


--
-- Name: console_node; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE console_node (
    id integer NOT NULL,
    status character varying(20) NOT NULL,
    eui character varying(50) NOT NULL,
    parent character varying(20) NOT NULL,
    name character varying(50) NOT NULL,
    rssi double precision NOT NULL,
    bat_level double precision NOT NULL,
    node_type character varying(50) NOT NULL,
    protocol character varying(50) NOT NULL,
    firmware_min character varying(50) NOT NULL,
    firmware_maj character varying(50) NOT NULL,
    library character varying(50) NOT NULL,
    status_seen character varying(50) NOT NULL,
    sensors character varying(50) NOT NULL,
    topology character varying(50) NOT NULL,
    gateway_id integer,
    send_status timestamp with time zone,
    public_id integer,
    reg_date timestamp with time zone NOT NULL
);


ALTER TABLE console_node OWNER TO postgres;

--
-- Name: console_node_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE console_node_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE console_node_id_seq OWNER TO postgres;

--
-- Name: console_node_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE console_node_id_seq OWNED BY console_node.id;


--
-- Name: console_nodebatteryusage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE console_nodebatteryusage (
    id integer NOT NULL,
    "timestamp" timestamp with time zone NOT NULL,
    level character varying(50),
    node_id integer
);


ALTER TABLE console_nodebatteryusage OWNER TO postgres;

--
-- Name: console_nodebattryusage_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE console_nodebattryusage_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE console_nodebattryusage_id_seq OWNER TO postgres;

--
-- Name: console_nodebattryusage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE console_nodebattryusage_id_seq OWNED BY console_nodebatteryusage.id;


--
-- Name: console_nodemodels; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE console_nodemodels (
    id integer NOT NULL,
    name character varying(200)
);


ALTER TABLE console_nodemodels OWNER TO postgres;

--
-- Name: console_nodemodels_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE console_nodemodels_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE console_nodemodels_id_seq OWNER TO postgres;

--
-- Name: console_nodemodels_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE console_nodemodels_id_seq OWNED BY console_nodemodels.id;


--
-- Name: console_rawmessage; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE console_rawmessage (
    id integer NOT NULL,
    body character varying(200),
    "timestamp" timestamp with time zone NOT NULL,
    ch character varying(2000),
    method character varying(2000),
    properties character varying(2000)
);


ALTER TABLE console_rawmessage OWNER TO postgres;

--
-- Name: console_rawmessage_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE console_rawmessage_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE console_rawmessage_id_seq OWNER TO postgres;

--
-- Name: console_rawmessage_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE console_rawmessage_id_seq OWNED BY console_rawmessage.id;


--
-- Name: console_sensor; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE console_sensor (
    id integer NOT NULL,
    name character varying(50) NOT NULL,
    node_type character varying(50) NOT NULL,
    status_seen character varying(20) NOT NULL,
    node_id integer,
    child_sensor_id integer,
    first_reg_date timestamp with time zone,
    reg_date timestamp with time zone
);


ALTER TABLE console_sensor OWNER TO postgres;

--
-- Name: console_sensor_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE console_sensor_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE console_sensor_id_seq OWNER TO postgres;

--
-- Name: console_sensor_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE console_sensor_id_seq OWNED BY console_sensor.id;


--
-- Name: console_sensoractualdata; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE console_sensoractualdata (
    id integer NOT NULL,
    receive_date timestamp with time zone NOT NULL,
    variable_type character varying(30) NOT NULL,
    value character varying(50),
    sensor_id integer
);


ALTER TABLE console_sensoractualdata OWNER TO postgres;

--
-- Name: console_sensoractualdata_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE console_sensoractualdata_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE console_sensoractualdata_id_seq OWNER TO postgres;

--
-- Name: console_sensoractualdata_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE console_sensoractualdata_id_seq OWNED BY console_sensoractualdata.id;


--
-- Name: console_sensordata; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE console_sensordata (
    id integer NOT NULL,
    variable_type character varying(30) NOT NULL,
    value character varying(50),
    sensor_id integer,
    receive_date timestamp with time zone NOT NULL
);


ALTER TABLE console_sensordata OWNER TO postgres;

--
-- Name: console_sensordata_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE console_sensordata_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE console_sensordata_id_seq OWNER TO postgres;

--
-- Name: console_sensordata_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE console_sensordata_id_seq OWNED BY console_sensordata.id;


--
-- Name: console_testinglog; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE console_testinglog (
    id integer NOT NULL,
    "timestamp" timestamp with time zone NOT NULL,
    device_name character varying(100),
    version character varying(100),
    test_number character varying(100),
    sta_mac character varying(100),
    sketch_md5 character varying(100)
);


ALTER TABLE console_testinglog OWNER TO postgres;

--
-- Name: console_testinglog_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE console_testinglog_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE console_testinglog_id_seq OWNER TO postgres;

--
-- Name: console_testinglog_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE console_testinglog_id_seq OWNED BY console_testinglog.id;


--
-- Name: django_admin_log; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE django_admin_log (
    id integer NOT NULL,
    action_time timestamp with time zone NOT NULL,
    object_id text,
    object_repr character varying(200) NOT NULL,
    action_flag smallint NOT NULL,
    change_message text NOT NULL,
    content_type_id integer,
    user_id integer NOT NULL,
    CONSTRAINT django_admin_log_action_flag_check CHECK ((action_flag >= 0))
);


ALTER TABLE django_admin_log OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE django_admin_log_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_admin_log_id_seq OWNER TO postgres;

--
-- Name: django_admin_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE django_admin_log_id_seq OWNED BY django_admin_log.id;


--
-- Name: django_content_type; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE django_content_type (
    id integer NOT NULL,
    app_label character varying(100) NOT NULL,
    model character varying(100) NOT NULL
);


ALTER TABLE django_content_type OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE django_content_type_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_content_type_id_seq OWNER TO postgres;

--
-- Name: django_content_type_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE django_content_type_id_seq OWNED BY django_content_type.id;


--
-- Name: django_migrations; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE django_migrations (
    id integer NOT NULL,
    app character varying(255) NOT NULL,
    name character varying(255) NOT NULL,
    applied timestamp with time zone NOT NULL
);


ALTER TABLE django_migrations OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE django_migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE django_migrations_id_seq OWNER TO postgres;

--
-- Name: django_migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE django_migrations_id_seq OWNED BY django_migrations.id;


--
-- Name: django_session; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE django_session (
    session_key character varying(40) NOT NULL,
    session_data text NOT NULL,
    expire_date timestamp with time zone NOT NULL
);


ALTER TABLE django_session OWNER TO postgres;

--
-- Name: auth_group id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group ALTER COLUMN id SET DEFAULT nextval('auth_group_id_seq'::regclass);


--
-- Name: auth_group_permissions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions ALTER COLUMN id SET DEFAULT nextval('auth_group_permissions_id_seq'::regclass);


--
-- Name: auth_permission id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_permission ALTER COLUMN id SET DEFAULT nextval('auth_permission_id_seq'::regclass);


--
-- Name: auth_user id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user ALTER COLUMN id SET DEFAULT nextval('auth_user_id_seq'::regclass);


--
-- Name: auth_user_groups id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_groups ALTER COLUMN id SET DEFAULT nextval('auth_user_groups_id_seq'::regclass);


--
-- Name: auth_user_user_permissions id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_user_permissions ALTER COLUMN id SET DEFAULT nextval('auth_user_user_permissions_id_seq'::regclass);


--
-- Name: console_appsettings id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_appsettings ALTER COLUMN id SET DEFAULT nextval('console_appsettings_id_seq'::regclass);


--
-- Name: console_appversion id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_appversion ALTER COLUMN id SET DEFAULT nextval('console_appversion_id_seq'::regclass);


--
-- Name: console_city id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_city ALTER COLUMN id SET DEFAULT nextval('console_city_id_seq'::regclass);


--
-- Name: console_client id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_client ALTER COLUMN id SET DEFAULT nextval('console_client_id_seq'::regclass);


--
-- Name: console_clientmessage id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_clientmessage ALTER COLUMN id SET DEFAULT nextval('console_clientmessage_id_seq'::regclass);


--
-- Name: console_clientmessagequeue id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_clientmessagequeue ALTER COLUMN id SET DEFAULT nextval('console_clientmessagequeue_id_seq'::regclass);


--
-- Name: console_connection id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_connection ALTER COLUMN id SET DEFAULT nextval('console_connection_id_seq'::regclass);


--
-- Name: console_firmware id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_firmware ALTER COLUMN id SET DEFAULT nextval('console_firmware_id_seq'::regclass);


--
-- Name: console_firmwarelog id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_firmwarelog ALTER COLUMN id SET DEFAULT nextval('console_firmwarelog_id_seq'::regclass);


--
-- Name: console_gateway id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_gateway ALTER COLUMN id SET DEFAULT nextval('console_gateway_id_seq'::regclass);


--
-- Name: console_heartbeattimer id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_heartbeattimer ALTER COLUMN id SET DEFAULT nextval('console_heartbeattimer_id_seq'::regclass);


--
-- Name: console_mqttmessage id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_mqttmessage ALTER COLUMN id SET DEFAULT nextval('console_mqttmessage_id_seq'::regclass);


--
-- Name: console_node id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_node ALTER COLUMN id SET DEFAULT nextval('console_node_id_seq'::regclass);


--
-- Name: console_nodebatteryusage id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_nodebatteryusage ALTER COLUMN id SET DEFAULT nextval('console_nodebattryusage_id_seq'::regclass);


--
-- Name: console_nodemodels id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_nodemodels ALTER COLUMN id SET DEFAULT nextval('console_nodemodels_id_seq'::regclass);


--
-- Name: console_rawmessage id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_rawmessage ALTER COLUMN id SET DEFAULT nextval('console_rawmessage_id_seq'::regclass);


--
-- Name: console_sensor id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_sensor ALTER COLUMN id SET DEFAULT nextval('console_sensor_id_seq'::regclass);


--
-- Name: console_sensoractualdata id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_sensoractualdata ALTER COLUMN id SET DEFAULT nextval('console_sensoractualdata_id_seq'::regclass);


--
-- Name: console_sensordata id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_sensordata ALTER COLUMN id SET DEFAULT nextval('console_sensordata_id_seq'::regclass);


--
-- Name: console_testinglog id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_testinglog ALTER COLUMN id SET DEFAULT nextval('console_testinglog_id_seq'::regclass);


--
-- Name: django_admin_log id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_admin_log ALTER COLUMN id SET DEFAULT nextval('django_admin_log_id_seq'::regclass);


--
-- Name: django_content_type id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_content_type ALTER COLUMN id SET DEFAULT nextval('django_content_type_id_seq'::regclass);


--
-- Name: django_migrations id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_migrations ALTER COLUMN id SET DEFAULT nextval('django_migrations_id_seq'::regclass);


--
-- Data for Name: auth_group; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_group (id, name) FROM stdin;
\.


--
-- Data for Name: auth_group_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_group_permissions (id, group_id, permission_id) FROM stdin;
\.


--
-- Data for Name: auth_permission; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_permission (id, name, content_type_id, codename) FROM stdin;
1	Can add client	1	add_client
2	Can change client	1	change_client
3	Can delete client	1	delete_client
4	Can add client message	2	add_clientmessage
5	Can change client message	2	change_clientmessage
6	Can delete client message	2	delete_clientmessage
7	Can add client message queue	3	add_clientmessagequeue
8	Can change client message queue	3	change_clientmessagequeue
9	Can delete client message queue	3	delete_clientmessagequeue
10	Can add connection	4	add_connection
11	Can change connection	4	change_connection
12	Can delete connection	4	delete_connection
13	Can add gateway	5	add_gateway
14	Can change gateway	5	change_gateway
15	Can delete gateway	5	delete_gateway
16	Can add node	6	add_node
17	Can change node	6	change_node
18	Can delete node	6	delete_node
19	Can add sensor	7	add_sensor
20	Can change sensor	7	change_sensor
21	Can delete sensor	7	delete_sensor
22	Can add raw message	8	add_rawmessage
23	Can change raw message	8	change_rawmessage
24	Can delete raw message	8	delete_rawmessage
25	Can add mqtt message	9	add_mqttmessage
26	Can change mqtt message	9	change_mqttmessage
27	Can delete mqtt message	9	delete_mqttmessage
28	Can add sensor data	10	add_sensordata
29	Can change sensor data	10	change_sensordata
30	Can delete sensor data	10	delete_sensordata
31	Can add app settings	11	add_appsettings
32	Can change app settings	11	change_appsettings
33	Can delete app settings	11	delete_appsettings
34	Can add city	12	add_city
35	Can change city	12	change_city
36	Can delete city	12	delete_city
37	Can add heartbeat timer	13	add_heartbeattimer
38	Can change heartbeat timer	13	change_heartbeattimer
39	Can delete heartbeat timer	13	delete_heartbeattimer
40	Can add node battery usage	14	add_nodebatteryusage
41	Can change node battery usage	14	change_nodebatteryusage
42	Can delete node battery usage	14	delete_nodebatteryusage
43	Can add log entry	15	add_logentry
44	Can change log entry	15	change_logentry
45	Can delete log entry	15	delete_logentry
46	Can add permission	16	add_permission
47	Can change permission	16	change_permission
48	Can delete permission	16	delete_permission
49	Can add group	17	add_group
50	Can change group	17	change_group
51	Can delete group	17	delete_group
52	Can add user	18	add_user
53	Can change user	18	change_user
54	Can delete user	18	delete_user
55	Can add content type	19	add_contenttype
56	Can change content type	19	change_contenttype
57	Can delete content type	19	delete_contenttype
58	Can add session	20	add_session
59	Can change session	20	change_session
60	Can delete session	20	delete_session
61	Can add firmware	21	add_firmware
62	Can change firmware	21	change_firmware
63	Can delete firmware	21	delete_firmware
64	Can add firmware log	22	add_firmwarelog
65	Can change firmware log	22	change_firmwarelog
66	Can delete firmware log	22	delete_firmwarelog
67	Can add node models	23	add_nodemodels
68	Can change node models	23	change_nodemodels
69	Can delete node models	23	delete_nodemodels
70	Can add testing log	24	add_testinglog
71	Can change testing log	24	change_testinglog
72	Can delete testing log	24	delete_testinglog
73	Can add sensor actual data	25	add_sensoractualdata
74	Can change sensor actual data	25	change_sensoractualdata
75	Can delete sensor actual data	25	delete_sensoractualdata
76	Can add app version	26	add_appversion
77	Can change app version	26	change_appversion
78	Can delete app version	26	delete_appversion
\.


--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_user (id, password, last_login, is_superuser, username, first_name, last_name, email, is_staff, is_active, date_joined) FROM stdin;
1	pbkdf2_sha256$36000$HUoo3oirVZzw$tMs8yjoyz4T0mkXTO5mlwKWxl72Hzwa+4IjWuaUmEj4=	2018-03-25 21:11:01.623034+07	t	admin				t	t	2018-02-09 23:15:19.307627+07
\.


--
-- Data for Name: auth_user_groups; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_user_groups (id, user_id, group_id) FROM stdin;
\.


--
-- Data for Name: auth_user_user_permissions; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auth_user_user_permissions (id, user_id, permission_id) FROM stdin;
\.


--
-- Data for Name: console_appsettings; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY console_appsettings (id, name, value, rus_name) FROM stdin;
1	timezone_for_console	Asia/Krasnoyarsk	Часовой пояс времени в консоле
2	status_seen_offset	-7	Смещение времени (работы Нодов)
3	nodes_check_interval	59	Интервал проверки доступности узлов (Heartbeat)
4	gateways_check_interval	33	Интервал поиска узлов (Discover)
5	INCLUSION_MODE	ON	Автоматическая регистрация Узлов
6	Testing db backup	this row should be hide after restore	Testing db backup
\.


--
-- Data for Name: console_appversion; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY console_appversion (id, "timestamp", number) FROM stdin;
\.


--
-- Data for Name: console_city; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY console_city (id, name, short_name, rus_name, timezone, latitude, longitude, population) FROM stdin;
\.


--
-- Data for Name: console_client; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY console_client (id, reg_date, name, email, phones, locale, status, city_id) FROM stdin;
1	2018-03-07 20:06:31.98964+07	Ilya	test@11test.com	9310010011	RU	1	\N
\.


--
-- Data for Name: console_clientmessage; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY console_clientmessage (id, text, channel) FROM stdin;
\.


--
-- Data for Name: console_clientmessagequeue; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY console_clientmessagequeue (id, create_date, status, delivery_date, client_id, message_id) FROM stdin;
\.


--
-- Data for Name: console_connection; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY console_connection (id, name, delay, network_type, qos, broker_host, topic_publish, topic_subscribe, user_login, password, client_id_id) FROM stdin;
1	gateway-18FE34DC046D	0	MQTT	0	mb1pub-gtw.hohobo.com	gateway-18FE34DC046D-in	gateway-18FE34DC046D-out			\N
2	gateway-5CCF7F0165DD	0	MQTT	0	mb1pub-gtw.hohobo.com	gateway-5CCF7F0165DD-in	gateway-5CCF7F0165DD-out			\N
3	gateway-18FE34E04256	0	MQTT	0	mb1pub-gtw.hohobo.com	gateway-18FE34E04256-in	gateway-18FE34E04256-out			\N
\.


--
-- Data for Name: console_firmware; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY console_firmware (id, name, document, is_latest, uploaded_at, uploaded_by_id, file_size, file_hash, firmware_type, file_type, node_model_id) FROM stdin;
20	firmware_1.0.604.wD00.es82.rf24.ino.bin		1	2018-02-14 01:05:08+07	1	8	8b652b8c79f357694a04bd793f533c96	firmware	1	\N
21	cl.csv		1	2018-02-14 23:32:49+07	1	2713	c60e4c6241e33b00ec926300b69fbd1e	smartconfig	1	\N
22	act-24.xlsx		1	2018-02-14 23:41:32.62752+07	1	7035	a45a86cffbbc327ea6f6e3af10f91cac	incorrect filename	1	\N
23	firmware.latest-2.bin		1	2018-02-14 23:41:55+07	1	4	5c9597f3c8245907ea71a89d9d39d08e	firmware	1	\N
\.


--
-- Data for Name: console_firmwarelog; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY console_firmwarelog (id, "timestamp", request_meta, request_data, responce_data, node_id, method) FROM stdin;
1	2018-02-12 23:51:12.38769+07		3984yr: 0		\N	\N
2	2018-02-12 23:51:56.363384+07		3984yr: 0		\N	\N
3	2018-02-13 23:16:57.242327+07		3984yr: 0		\N	\N
4	2018-02-13 23:31:56.627619+07		3984yr: 0		\N	\N
5	2018-02-14 01:15:54.237831+07		no mac: None		\N	\N
6	2018-02-14 01:16:01.463119+07		no mac: None		\N	\N
7	2018-02-14 01:16:25.562147+07		no mac: 1		\N	\N
8	2018-02-14 01:17:17.151919+07		no mac: 1		\N	\N
9	2018-02-14 01:17:29.998146+07		no mac: 1		\N	\N
10	2018-02-14 01:17:55.899102+07		no mac: firmware_1.0.604.wD00.es82.rf24.ino.bin		\N	\N
11	2018-02-14 01:18:00.171168+07		no mac: firmware_1.0.604.wD00.es82.rf24.ino.bin		\N	\N
12	2018-02-14 01:18:33.867472+07		no mac: firmware_1.0.604.wD00.es82.rf24.ino.bin		\N	\N
13	2018-02-14 01:18:43.375787+07		no mac: 1		\N	\N
14	2018-02-15 21:30:00.666713+07	PATH:/Users/davydov/projects/fox/env/bin:/Library/Frameworks/Python.framework/Versions/3.6/bin:/usr/local/bin:/usr/bin:/bin:/usr/sbin:/sbin:/Applications/Postgres.app/Contents/Versions/latest/bin:/Library/Frameworks/Python.framework/Versions/3.6/bin;PATH_INFO:/firmware/firmware.bin	no mac: .0: no hash	8b652b8c79f357694a04bd793f533c96: firmware_1.0.604.wD00.es82.rf24.ino.bin	\N	\N
15	2018-02-15 21:36:23.18998+07		no mac: .0: no hash	8b652b8c79f357694a04bd793f533c96: firmware_1.0.604.wD00.es82.rf24.ino.bin	\N	/firmware/firmware.bin
16	2018-02-15 21:46:38.367212+07		no mac: .0: no hash	c60e4c6241e33b00ec926300b69fbd1e: cl.csv	\N	/firmware/smartconfig.bin
17	2018-02-22 20:55:43.24393+07		no mac: .0: no hash	firmware not found	\N	/update/
18	2018-02-22 20:55:46.456172+07		no mac: .0: no hash	firmware not found	\N	/update/
19	2018-02-22 20:55:48.526459+07		no mac: .0: no hash	firmware not found	\N	/update/
\.


--
-- Data for Name: console_gateway; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY console_gateway (id, status_broker, mobile, web, uid_gateway, status_seen, seen_message, nodes_sensors, topology, connection_id, name, client_id, status, inclusion_mode, reg_date) FROM stdin;
2	\N	\N	\N	\N	\N	\N	\N	\N	2	gateway-5CCF7F0165DD	\N	New	0	2018-02-27 22:49:07.450436+07
3	\N	\N	\N	\N	\N	\N	\N	\N	3	gateway-18FE34E04256	1	Registered	0	2018-02-27 22:49:07.450436+07
4	\N	\N	\N	\N	\N	\N	\N	\N	1	gateway-18FE34DC046D	1	Registered	0	2018-03-07 20:09:32.370197+07
\.


--
-- Data for Name: console_heartbeattimer; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY console_heartbeattimer (id, object_type, last_check_date) FROM stdin;
\.


--
-- Data for Name: console_mqttmessage; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY console_mqttmessage (id, "timestamp", consumer_tag, delivery_tag, exchange, redelivered, uri, cluster_name, routing_key, payload, gateway, direction, node_id, command, ack, type, child_sensor_id, dup, qos) FROM stdin;
\.


--
-- Data for Name: console_node; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY console_node (id, status, eui, parent, name, rssi, bat_level, node_type, protocol, firmware_min, firmware_maj, library, status_seen, sensors, topology, gateway_id, send_status, public_id, reg_date) FROM stdin;
3	Registered	gateway-18FE34E04256-out/0			0	0									3	2018-03-07 20:10:33.714013+07	0	2018-02-27 22:49:07.507848+07
2	New	gateway-5CCF7F0165DD-out/0			0	100						187310875			2	2018-03-07 20:11:05.904263+07	0	2018-02-27 22:49:07.507848+07
6	Registered	gateway-18FE34DC046D-out/7			0	0									4	2018-03-07 20:09:45.598688+07	7	2018-03-07 20:09:45.595655+07
8	Registered	gateway-18FE34DC046D-out/10			0	0									4	2018-03-07 20:10:37.809526+07	10	2018-03-07 20:10:37.806477+07
7	Registered	gateway-18FE34DC046D-out/5			0	0									4	2018-03-07 20:11:04.07402+07	5	2018-03-07 20:09:47.424598+07
5	Registered	gateway-18FE34DC046D-out/0	1	HO-Gate HWG-100	80	92	Repeater Node	test	1.0.8-B4.wD00.es82.rf24	test	2.2.0	695543598	test	test	4	2018-03-07 20:09:32+07	0	2018-03-07 20:09:32.375548+07
\.


--
-- Data for Name: console_nodebatteryusage; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY console_nodebatteryusage (id, "timestamp", level, node_id) FROM stdin;
1	2018-02-27 21:23:40.654694+07	81	2
2	2018-02-27 21:24:15.108445+07	87	2
3	2018-03-07 20:10:45.601844+07	100	2
4	2018-03-07 20:11:05.563422+07	100	2
\.


--
-- Data for Name: console_nodemodels; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY console_nodemodels (id, name) FROM stdin;
\.


--
-- Data for Name: console_rawmessage; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY console_rawmessage (id, body, "timestamp", ch, method, properties) FROM stdin;
\.


--
-- Data for Name: console_sensor; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY console_sensor (id, name, node_type, status_seen, node_id, child_sensor_id, first_reg_date, reg_date) FROM stdin;
3	NoPresentation-gateway-5CCF7F0165DD-out/0-3			2	3	2018-02-27 22:57:13.127655+07	2018-02-27 23:03:13.750448+07
4	NoPresentation-gateway-18FE34E04256-out/0-1			3	1	2018-02-27 22:57:13.127655+07	2018-02-27 23:03:13.750448+07
5	NoPresentation-gateway-5CCF7F0165DD-out/0-13			2	13	2018-02-27 22:57:13.127655+07	2018-02-27 23:03:13.750448+07
6	NoPresentation-gateway-5CCF7F0165DD-out/0-0			2	0	2018-02-27 22:57:13.127655+07	2018-02-27 23:03:13.750448+07
7	NoPresentation-gateway-5CCF7F0165DD-out/0-1			2	1	2018-02-27 22:57:13.127655+07	2018-02-27 23:03:13.750448+07
8	NoPresentation-gateway-5CCF7F0165DD-out/0-2			2	2	2018-02-27 22:57:13.127655+07	2018-02-27 23:03:13.750448+07
10	NoPresentation-gateway-18FE34DC046D-out/0-1			5	1	2018-03-07 20:09:32.383082+07	2018-03-07 20:09:32.383035+07
11	NoPresentation-gateway-18FE34DC046D-out/7-2			6	2	2018-03-07 20:09:45.602625+07	2018-03-07 20:09:45.602575+07
12	NoPresentation-gateway-18FE34DC046D-out/5-1			7	1	2018-03-07 20:09:47.431968+07	2018-03-07 20:09:47.43192+07
13	NoPresentation-gateway-18FE34DC046D-out/5-0			7	0	2018-03-07 20:09:48.124869+07	2018-03-07 20:09:48.124822+07
14	NoPresentation-gateway-18FE34DC046D-out/10-0			8	0	2018-03-07 20:10:37.81342+07	2018-03-07 20:10:37.81337+07
\.


--
-- Data for Name: console_sensoractualdata; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY console_sensoractualdata (id, receive_date, variable_type, value, sensor_id) FROM stdin;
\.


--
-- Data for Name: console_sensordata; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY console_sensordata (id, variable_type, value, sensor_id, receive_date) FROM stdin;
3	V_VOLTAGE	4.09	3	2018-02-27 21:23:40.997363+07
9	V_VOLTAGE	278	4	2018-02-27 21:24:05.957777+07
10	V_VOLUME	1	5	2018-02-27 21:24:13.416543+07
11	V_VAR5	-50	6	2018-02-27 21:24:14.087571+07
12	V_VOLTAGE	0	7	2018-02-27 21:24:14.423182+07
13	V_LIGHT_LEVEL	57	8	2018-02-27 21:24:14.765226+07
14	V_VOLTAGE	4.12	3	2018-02-27 21:24:15.442289+07
18	V_VOLTAGE	292	4	2018-02-27 21:24:36.055425+07
26	V_VOLTAGE	272	4	2018-03-07 20:09:31.688464+07
28	V_VOLTAGE	276	4	2018-03-07 20:09:39.659833+07
29	V_VOLTAGE	289	4	2018-03-07 20:09:40.712076+07
30	V_WIND	2.5	11	2018-03-07 20:09:45.605639+07
31	V_VOLTAGE	228	12	2018-03-07 20:09:47.792421+07
32	V_WATT	572.8	13	2018-03-07 20:09:48.127965+07
33	V_VOLTAGE	275	4	2018-03-07 20:10:33.719512+07
34	V_STATUS	0	14	2018-03-07 20:10:37.816339+07
35	V_VOLTAGE	4.25	3	2018-03-07 20:10:45.942392+07
36	V_VOLTAGE	228	12	2018-03-07 20:11:03.742791+07
37	V_WATT	513.2	13	2018-03-07 20:11:04.079043+07
38	V_VOLTAGE	4.24	3	2018-03-07 20:11:05.90867+07
27	V_VOLTAGE	220	10	2018-03-07 20:09:32+07
\.


--
-- Data for Name: console_testinglog; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY console_testinglog (id, "timestamp", device_name, version, test_number, sta_mac, sketch_md5) FROM stdin;
1	2018-02-22 21:49:50.912281+07	\N	\N	\N	\N	\N
\.


--
-- Data for Name: django_admin_log; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY django_admin_log (id, action_time, object_id, object_repr, action_flag, change_message, content_type_id, user_id) FROM stdin;
1	2018-02-09 23:52:14.657198+07	1	AppSettings object	1	[{"added": {}}]	11	1
2	2018-02-09 23:52:29.323042+07	1	AppSettings object	2	[{"changed": {"fields": ["rus_name"]}}]	11	1
3	2018-02-09 23:52:55.698868+07	2	AppSettings object	1	[{"added": {}}]	11	1
4	2018-02-09 23:53:28.328351+07	3	AppSettings object	1	[{"added": {}}]	11	1
5	2018-02-09 23:53:53.384174+07	4	AppSettings object	1	[{"added": {}}]	11	1
6	2018-02-12 23:42:45.753181+07	1	new firmware	1	[{"added": {}}]	21	1
7	2018-02-12 23:43:38.34774+07	2	old firmware	1	[{"added": {}}]	21	1
8	2018-02-12 23:43:50.12433+07	1	new firmware	2	[]	21	1
9	2018-02-12 23:44:04.957245+07	2	old firmware	2	[]	21	1
10	2018-02-14 00:27:24.36524+07	2	old firmware	3		21	1
11	2018-02-14 00:27:24.376036+07	1	new firmware	3		21	1
12	2018-02-14 00:29:50.561506+07	5		2	[{"changed": {"fields": ["document"]}}]	21	1
13	2018-02-14 00:39:16.457003+07	6		3		21	1
14	2018-02-14 00:39:16.461383+07	5		3		21	1
15	2018-02-14 00:39:16.463637+07	4		3		21	1
16	2018-02-14 00:39:16.465707+07	3		3		21	1
17	2018-02-14 00:50:21.221605+07	13	firmware.latest-2.bin	3		21	1
18	2018-02-14 00:50:21.225362+07	12	firmware.latest-2.bin	3		21	1
19	2018-02-14 00:50:21.227006+07	11	pl.xlsx	3		21	1
20	2018-02-14 00:50:21.229017+07	10	firmware.latest-2.bin	3		21	1
21	2018-02-14 00:50:21.230816+07	9	package-lock.json	3		21	1
22	2018-02-14 00:50:21.232762+07	8	package.json	3		21	1
23	2018-02-14 00:50:21.234507+07	7	invoice.docx	3		21	1
24	2018-02-14 01:00:51.440325+07	18	firmware.latest.bin	3		21	1
25	2018-02-14 01:00:51.445231+07	17	act-24.xlsx	3		21	1
26	2018-02-14 01:00:51.447316+07	16	invoice.docx	3		21	1
27	2018-02-14 01:00:51.449096+07	15	firmware.latest.bin	3		21	1
28	2018-02-14 01:00:51.450776+07	14	act-24.xlsx	3		21	1
29	2018-02-14 01:05:51.197747+07	20	firmware_1.0.604.wD00.es82.rf24.ino.bin	2	[{"changed": {"fields": ["is_latest"]}}]	21	1
30	2018-02-14 01:24:00.975792+07	19	firmware.latest-2.bin	3		21	1
31	2018-02-14 23:33:17.159773+07	21	cl.csv	2	[{"changed": {"fields": ["firmware_type"]}}]	21	1
32	2018-02-14 23:33:30.916411+07	20	firmware_1.0.604.wD00.es82.rf24.ino.bin	2	[{"changed": {"fields": ["firmware_type"]}}]	21	1
33	2018-02-14 23:36:13.63106+07	21	cl.csv	2	[{"changed": {"fields": ["is_latest"]}}]	21	1
34	2018-02-14 23:42:02.522997+07	23	firmware.latest-2.bin	2	[]	21	1
35	2018-02-27 21:25:54.935206+07	5	AppSettings object	1	[{"added": {}}]	11	1
36	2018-02-27 21:26:18.36831+07	5	AppSettings object	2	[]	11	1
37	2018-03-06 22:13:05.822689+07	1	gateway-18FE34DC046D	3		5	1
38	2018-03-07 20:06:31.996013+07	1	Ilya	1	[{"added": {}}]	1	1
39	2018-03-07 20:06:43.516113+07	3	gateway-18FE34E04256	2	[{"changed": {"fields": ["client"]}}]	5	1
40	2018-03-07 20:11:19.53693+07	4	gateway-18FE34DC046D	2	[{"changed": {"fields": ["client"]}}]	5	1
41	2018-03-07 20:22:20.687714+07	5	HO-Gate HWG-100	2	[{"changed": {"fields": ["parent", "name", "rssi", "bat_level", "node_type", "protocol", "firmware_min", "firmware_maj", "library", "status_seen", "sensors", "topology"]}}]	6	1
42	2018-03-10 13:35:04.924994+07	5	HO-Gate HWG-100	2	[{"changed": {"fields": ["rssi"]}}]	6	1
43	2018-03-10 13:46:43.166141+07	1	Ilya	2	[{"changed": {"fields": ["email"]}}]	1	1
44	2018-03-10 13:47:18.165788+07	5	HO-Gate HWG-100	2	[{"changed": {"fields": ["rssi"]}}]	6	1
45	2018-03-10 13:48:24.547815+07	27	V_VOLTAGE:220	2	[{"changed": {"fields": ["value"]}}]	10	1
46	2018-03-25 21:12:04.444046+07	6	AppSettings object	1	[{"added": {}}]	11	1
\.


--
-- Data for Name: django_content_type; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY django_content_type (id, app_label, model) FROM stdin;
1	console	client
2	console	clientmessage
3	console	clientmessagequeue
4	console	connection
5	console	gateway
6	console	node
7	console	sensor
8	console	rawmessage
9	console	mqttmessage
10	console	sensordata
11	console	appsettings
12	console	city
13	console	heartbeattimer
14	console	nodebatteryusage
15	admin	logentry
16	auth	permission
17	auth	group
18	auth	user
19	contenttypes	contenttype
20	sessions	session
21	console	firmware
22	console	firmwarelog
23	console	nodemodels
24	console	testinglog
25	console	sensoractualdata
26	console	appversion
\.


--
-- Data for Name: django_migrations; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY django_migrations (id, app, name, applied) FROM stdin;
1	contenttypes	0001_initial	2018-02-09 23:13:38.278649+07
2	auth	0001_initial	2018-02-09 23:13:38.356729+07
3	admin	0001_initial	2018-02-09 23:13:38.383567+07
4	admin	0002_logentry_remove_auto_add	2018-02-09 23:13:38.399874+07
5	contenttypes	0002_remove_content_type_name	2018-02-09 23:13:38.423594+07
6	auth	0002_alter_permission_name_max_length	2018-02-09 23:13:38.430686+07
7	auth	0003_alter_user_email_max_length	2018-02-09 23:13:38.444252+07
8	auth	0004_alter_user_username_opts	2018-02-09 23:13:38.457668+07
9	auth	0005_alter_user_last_login_null	2018-02-09 23:13:38.473188+07
10	auth	0006_require_contenttypes_0002	2018-02-09 23:13:38.475857+07
11	auth	0007_alter_validators_add_error_messages	2018-02-09 23:13:38.489761+07
12	auth	0008_alter_user_username_max_length	2018-02-09 23:13:38.507309+07
13	console	0001_initial	2018-02-09 23:13:38.580906+07
14	console	0002_gateway_name	2018-02-09 23:13:38.590467+07
15	console	0003_rowmessage	2018-02-09 23:13:38.600945+07
16	console	0004_auto_20171108_2332	2018-02-09 23:13:38.611237+07
17	console	0005_auto_20171108_2340	2018-02-09 23:13:38.634705+07
18	console	0006_auto_20171108_2344	2018-02-09 23:13:38.655224+07
19	console	0007_auto_20171108_2348	2018-02-09 23:13:38.661162+07
20	console	0008_auto_20171108_2349	2018-02-09 23:13:38.671338+07
21	console	0009_mqttmessage	2018-02-09 23:13:38.681421+07
22	console	0010_auto_20171113_2246	2018-02-09 23:13:38.715891+07
23	console	0011_auto_20171113_2331	2018-02-09 23:13:38.723691+07
24	console	0012_auto_20171114_2240	2018-02-09 23:13:38.757577+07
25	console	0013_client_gateway	2018-02-09 23:13:38.772056+07
26	console	0014_auto_20171130_2259	2018-02-09 23:13:38.813944+07
27	console	0015_node_gateway	2018-02-09 23:13:38.828102+07
28	console	0016_auto_20171209_0801	2018-02-09 23:13:38.895705+07
29	console	0017_auto_20171209_1152	2018-02-09 23:13:38.92081+07
30	console	0018_auto_20171209_1340	2018-02-09 23:13:38.932002+07
31	console	0019_auto_20171209_1351	2018-02-09 23:13:38.991044+07
32	console	0020_auto_20171210_1540	2018-02-09 23:13:38.999362+07
33	console	0021_auto_20171211_2103	2018-02-09 23:13:39.011474+07
34	console	0022_auto_20171212_2302	2018-02-09 23:13:39.046584+07
35	console	0023_auto_20171213_0033	2018-02-09 23:13:39.061983+07
36	console	0024_auto_20171213_2249	2018-02-09 23:13:39.104281+07
37	console	0025_auto_20171213_2333	2018-02-09 23:13:39.117049+07
38	console	0026_remove_node_send_status	2018-02-09 23:13:39.126718+07
39	console	0027_auto_20171213_2335	2018-02-09 23:13:39.139873+07
40	console	0028_auto_20171214_2132	2018-02-09 23:13:39.15185+07
41	console	0029_auto_20171214_2200	2018-02-09 23:13:39.163988+07
42	console	0030_auto_20171214_2223	2018-02-09 23:13:39.18093+07
43	console	0031_auto_20171217_1459	2018-02-09 23:13:39.217785+07
44	console	0032_sensor_child_sensor_id	2018-02-09 23:13:39.227882+07
45	console	0033_sensordata_receive_date	2018-02-09 23:13:39.240014+07
46	console	0034_auto_20171218_2023	2018-02-09 23:13:39.248945+07
47	console	0035_gateway_status	2018-02-09 23:13:39.270693+07
48	console	0036_auto_20171221_1916	2018-02-09 23:13:39.284175+07
49	console	0037_appsettings	2018-02-09 23:13:39.294582+07
50	console	0038_city	2018-02-09 23:13:39.306784+07
51	console	0039_client_city	2018-02-09 23:13:39.324128+07
52	console	0040_appsettings_rus_name	2018-02-09 23:13:39.329767+07
53	console	0041_timeresponce	2018-02-09 23:13:39.339421+07
54	console	0042_delete_timeresponce	2018-02-09 23:13:39.345221+07
55	console	0043_node_node_id	2018-02-09 23:13:39.358865+07
56	console	0044_auto_20180103_1719	2018-02-09 23:13:39.37356+07
57	console	0045_auto_20180103_1856	2018-02-09 23:13:39.382496+07
58	console	0046_heartbeattimer	2018-02-09 23:13:39.395211+07
59	console	0047_nodebattryusage	2018-02-09 23:13:39.415886+07
60	console	0048_auto_20180115_2245	2018-02-09 23:13:39.450629+07
61	sessions	0001_initial	2018-02-09 23:13:39.461961+07
62	console	0049_firmware_firmwarelog	2018-02-12 23:40:33.588937+07
63	console	0050_auto_20180212_2342	2018-02-12 23:42:37.433583+07
64	console	0051_auto_20180214_0005	2018-02-14 00:05:25.663217+07
65	console	0052_auto_20180214_0014	2018-02-14 00:14:56.663564+07
66	console	0053_firmware_file_hash	2018-02-14 00:21:15.236579+07
67	console	0054_auto_20180214_0028	2018-02-14 00:28:58.517738+07
68	console	0055_firmware_firmware_type	2018-02-14 23:32:13.712958+07
69	console	0056_auto_20180215_2128	2018-02-15 21:28:13.611919+07
70	console	0057_firmwarelog_method	2018-02-15 21:32:58.869946+07
71	console	0058_auto_20180215_2136	2018-02-15 21:36:17.024128+07
72	console	0059_auto_20180220_2145	2018-02-20 21:45:53.179931+07
73	console	0060_auto_20180220_2336	2018-02-20 23:36:48.263038+07
74	console	0061_nodemodels	2018-02-21 00:04:07.90644+07
75	console	0062_auto_20180221_0004	2018-02-21 00:04:44.800401+07
76	console	0063_auto_20180221_0032	2018-02-21 00:33:03.070701+07
77	console	0064_testinglog	2018-02-22 21:39:54.739836+07
78	console	0065_node_id_flag	2018-02-25 22:47:59.663745+07
79	console	0066_auto_20180226_0812	2018-02-26 08:13:00.288154+07
80	console	0067_auto_20180227_2248	2018-02-27 22:49:07.535883+07
81	console	0068_auto_20180227_2257	2018-02-27 22:57:13.13931+07
82	console	0069_sensor_reg_date	2018-02-27 23:03:13.760544+07
83	console	0070_auto_20180301_2044	2018-03-01 20:44:29.446651+07
84	console	0071_sensoractualdata	2018-03-11 22:39:22.966935+07
85	console	0072_auto_20180311_2253	2018-03-11 22:53:42.372134+07
86	console	0073_auto_20180311_2301	2018-03-11 23:02:01.911777+07
87	console	0074_appversion	2018-03-18 23:35:46.732564+07
\.


--
-- Data for Name: django_session; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY django_session (session_key, session_data, expire_date) FROM stdin;
0l8d2kpb0bvbc396vpe051vtc6568w8h	NjMwMTE0OWJiMWEzMGE3M2VkMGY5MWZmYzViMTM1MzI4MzA2ZTdmNjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJkMTRmNTExOWZlOWZmODc2ODE4MGM3MDgxMDIwZGVmMTQxZjZiMDBjIn0=	2018-02-23 23:16:19.753172+07
wxisutegiv89d36onp77wzk85os11cmi	NjMwMTE0OWJiMWEzMGE3M2VkMGY5MWZmYzViMTM1MzI4MzA2ZTdmNjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJkMTRmNTExOWZlOWZmODc2ODE4MGM3MDgxMDIwZGVmMTQxZjZiMDBjIn0=	2018-02-26 23:41:07.787102+07
nm9azbpz30vd2ip4srfdmubjjankicf1	NjMwMTE0OWJiMWEzMGE3M2VkMGY5MWZmYzViMTM1MzI4MzA2ZTdmNjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJkMTRmNTExOWZlOWZmODc2ODE4MGM3MDgxMDIwZGVmMTQxZjZiMDBjIn0=	2018-02-28 00:42:14.825915+07
7b9g27869ttt3s7bpd1lufywkn7lh6pe	NjMwMTE0OWJiMWEzMGE3M2VkMGY5MWZmYzViMTM1MzI4MzA2ZTdmNjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJkMTRmNTExOWZlOWZmODc2ODE4MGM3MDgxMDIwZGVmMTQxZjZiMDBjIn0=	2018-03-13 21:25:21.879912+07
780bdgqcl8rdnw89iweqsujn79489xhc	NjMwMTE0OWJiMWEzMGE3M2VkMGY5MWZmYzViMTM1MzI4MzA2ZTdmNjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJkMTRmNTExOWZlOWZmODc2ODE4MGM3MDgxMDIwZGVmMTQxZjZiMDBjIn0=	2018-03-21 20:05:38.155899+07
a4twq365jm2yq0ctrr8gfdryoor1qsix	NjMwMTE0OWJiMWEzMGE3M2VkMGY5MWZmYzViMTM1MzI4MzA2ZTdmNjp7Il9hdXRoX3VzZXJfaWQiOiIxIiwiX2F1dGhfdXNlcl9iYWNrZW5kIjoiZGphbmdvLmNvbnRyaWIuYXV0aC5iYWNrZW5kcy5Nb2RlbEJhY2tlbmQiLCJfYXV0aF91c2VyX2hhc2giOiJkMTRmNTExOWZlOWZmODc2ODE4MGM3MDgxMDIwZGVmMTQxZjZiMDBjIn0=	2018-04-08 21:11:01.627339+07
\.


--
-- Name: auth_group_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_group_id_seq', 1, false);


--
-- Name: auth_group_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_group_permissions_id_seq', 1, false);


--
-- Name: auth_permission_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_permission_id_seq', 78, true);


--
-- Name: auth_user_groups_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_user_groups_id_seq', 1, false);


--
-- Name: auth_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_user_id_seq', 1, true);


--
-- Name: auth_user_user_permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auth_user_user_permissions_id_seq', 1, false);


--
-- Name: console_appsettings_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('console_appsettings_id_seq', 5, true);


--
-- Name: console_appversion_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('console_appversion_id_seq', 1, false);


--
-- Name: console_city_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('console_city_id_seq', 1, false);


--
-- Name: console_client_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('console_client_id_seq', 1, true);


--
-- Name: console_clientmessage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('console_clientmessage_id_seq', 1, false);


--
-- Name: console_clientmessagequeue_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('console_clientmessagequeue_id_seq', 1, false);


--
-- Name: console_connection_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('console_connection_id_seq', 3, true);


--
-- Name: console_firmware_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('console_firmware_id_seq', 23, true);


--
-- Name: console_firmwarelog_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('console_firmwarelog_id_seq', 19, true);


--
-- Name: console_gateway_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('console_gateway_id_seq', 4, true);


--
-- Name: console_heartbeattimer_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('console_heartbeattimer_id_seq', 1, false);


--
-- Name: console_mqttmessage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('console_mqttmessage_id_seq', 1, false);


--
-- Name: console_node_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('console_node_id_seq', 8, true);


--
-- Name: console_nodebattryusage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('console_nodebattryusage_id_seq', 4, true);


--
-- Name: console_nodemodels_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('console_nodemodels_id_seq', 1, false);


--
-- Name: console_rawmessage_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('console_rawmessage_id_seq', 1, false);


--
-- Name: console_sensor_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('console_sensor_id_seq', 14, true);


--
-- Name: console_sensoractualdata_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('console_sensoractualdata_id_seq', 1, false);


--
-- Name: console_sensordata_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('console_sensordata_id_seq', 38, true);


--
-- Name: console_testinglog_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('console_testinglog_id_seq', 1, true);


--
-- Name: django_admin_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('django_admin_log_id_seq', 45, true);


--
-- Name: django_content_type_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('django_content_type_id_seq', 26, true);


--
-- Name: django_migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('django_migrations_id_seq', 87, true);


--
-- Name: auth_group auth_group_name_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_name_key UNIQUE (name);


--
-- Name: auth_group_permissions auth_group_permissions_group_id_permission_id_0cd325b0_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_permission_id_0cd325b0_uniq UNIQUE (group_id, permission_id);


--
-- Name: auth_group_permissions auth_group_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_group auth_group_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group
    ADD CONSTRAINT auth_group_pkey PRIMARY KEY (id);


--
-- Name: auth_permission auth_permission_content_type_id_codename_01ab375a_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_codename_01ab375a_uniq UNIQUE (content_type_id, codename);


--
-- Name: auth_permission auth_permission_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_pkey PRIMARY KEY (id);


--
-- Name: auth_user_groups auth_user_groups_user_id_group_id_94350c0c_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_group_id_94350c0c_uniq UNIQUE (user_id, group_id);


--
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_pkey PRIMARY KEY (id);


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_permission_id_14a6b632_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_permission_id_14a6b632_uniq UNIQUE (user_id, permission_id);


--
-- Name: auth_user auth_user_username_key; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user
    ADD CONSTRAINT auth_user_username_key UNIQUE (username);


--
-- Name: console_appsettings console_appsettings_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_appsettings
    ADD CONSTRAINT console_appsettings_pkey PRIMARY KEY (id);


--
-- Name: console_appversion console_appversion_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_appversion
    ADD CONSTRAINT console_appversion_pkey PRIMARY KEY (id);


--
-- Name: console_city console_city_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_city
    ADD CONSTRAINT console_city_pkey PRIMARY KEY (id);


--
-- Name: console_client console_client_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_client
    ADD CONSTRAINT console_client_pkey PRIMARY KEY (id);


--
-- Name: console_clientmessage console_clientmessage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_clientmessage
    ADD CONSTRAINT console_clientmessage_pkey PRIMARY KEY (id);


--
-- Name: console_clientmessagequeue console_clientmessagequeue_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_clientmessagequeue
    ADD CONSTRAINT console_clientmessagequeue_pkey PRIMARY KEY (id);


--
-- Name: console_connection console_connection_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_connection
    ADD CONSTRAINT console_connection_pkey PRIMARY KEY (id);


--
-- Name: console_firmware console_firmware_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_firmware
    ADD CONSTRAINT console_firmware_pkey PRIMARY KEY (id);


--
-- Name: console_firmwarelog console_firmwarelog_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_firmwarelog
    ADD CONSTRAINT console_firmwarelog_pkey PRIMARY KEY (id);


--
-- Name: console_gateway console_gateway_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_gateway
    ADD CONSTRAINT console_gateway_pkey PRIMARY KEY (id);


--
-- Name: console_heartbeattimer console_heartbeattimer_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_heartbeattimer
    ADD CONSTRAINT console_heartbeattimer_pkey PRIMARY KEY (id);


--
-- Name: console_mqttmessage console_mqttmessage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_mqttmessage
    ADD CONSTRAINT console_mqttmessage_pkey PRIMARY KEY (id);


--
-- Name: console_node console_node_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_node
    ADD CONSTRAINT console_node_pkey PRIMARY KEY (id);


--
-- Name: console_nodebatteryusage console_nodebattryusage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_nodebatteryusage
    ADD CONSTRAINT console_nodebattryusage_pkey PRIMARY KEY (id);


--
-- Name: console_nodemodels console_nodemodels_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_nodemodels
    ADD CONSTRAINT console_nodemodels_pkey PRIMARY KEY (id);


--
-- Name: console_rawmessage console_rawmessage_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_rawmessage
    ADD CONSTRAINT console_rawmessage_pkey PRIMARY KEY (id);


--
-- Name: console_sensor console_sensor_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_sensor
    ADD CONSTRAINT console_sensor_pkey PRIMARY KEY (id);


--
-- Name: console_sensoractualdata console_sensoractualdata_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_sensoractualdata
    ADD CONSTRAINT console_sensoractualdata_pkey PRIMARY KEY (id);


--
-- Name: console_sensordata console_sensordata_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_sensordata
    ADD CONSTRAINT console_sensordata_pkey PRIMARY KEY (id);


--
-- Name: console_testinglog console_testinglog_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_testinglog
    ADD CONSTRAINT console_testinglog_pkey PRIMARY KEY (id);


--
-- Name: django_admin_log django_admin_log_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_pkey PRIMARY KEY (id);


--
-- Name: django_content_type django_content_type_app_label_model_76bd3d3b_uniq; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_app_label_model_76bd3d3b_uniq UNIQUE (app_label, model);


--
-- Name: django_content_type django_content_type_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_content_type
    ADD CONSTRAINT django_content_type_pkey PRIMARY KEY (id);


--
-- Name: django_migrations django_migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_migrations
    ADD CONSTRAINT django_migrations_pkey PRIMARY KEY (id);


--
-- Name: django_session django_session_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_session
    ADD CONSTRAINT django_session_pkey PRIMARY KEY (session_key);


--
-- Name: auth_group_name_a6ea08ec_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_name_a6ea08ec_like ON auth_group USING btree (name varchar_pattern_ops);


--
-- Name: auth_group_permissions_group_id_b120cbf9; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_permissions_group_id_b120cbf9 ON auth_group_permissions USING btree (group_id);


--
-- Name: auth_group_permissions_permission_id_84c5c92e; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_group_permissions_permission_id_84c5c92e ON auth_group_permissions USING btree (permission_id);


--
-- Name: auth_permission_content_type_id_2f476e4b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_permission_content_type_id_2f476e4b ON auth_permission USING btree (content_type_id);


--
-- Name: auth_user_groups_group_id_97559544; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_groups_group_id_97559544 ON auth_user_groups USING btree (group_id);


--
-- Name: auth_user_groups_user_id_6a12ed8b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_groups_user_id_6a12ed8b ON auth_user_groups USING btree (user_id);


--
-- Name: auth_user_user_permissions_permission_id_1fbb5f2c; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_user_permissions_permission_id_1fbb5f2c ON auth_user_user_permissions USING btree (permission_id);


--
-- Name: auth_user_user_permissions_user_id_a95ead1b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_user_permissions_user_id_a95ead1b ON auth_user_user_permissions USING btree (user_id);


--
-- Name: auth_user_username_6821ab7c_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX auth_user_username_6821ab7c_like ON auth_user USING btree (username varchar_pattern_ops);


--
-- Name: console_client_city_id_207ed370; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX console_client_city_id_207ed370 ON console_client USING btree (city_id);


--
-- Name: console_clientmessagequeue_client_id_1f1d6530; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX console_clientmessagequeue_client_id_1f1d6530 ON console_clientmessagequeue USING btree (client_id);


--
-- Name: console_clientmessagequeue_message_id_7d6deb89; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX console_clientmessagequeue_message_id_7d6deb89 ON console_clientmessagequeue USING btree (message_id);


--
-- Name: console_connection_client_id_id_9301b35b; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX console_connection_client_id_id_9301b35b ON console_connection USING btree (client_id_id);


--
-- Name: console_firmware_node_model_id_0e9774ee; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX console_firmware_node_model_id_0e9774ee ON console_firmware USING btree (node_model_id);


--
-- Name: console_firmware_uploaded_by_id_e231fb2a; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX console_firmware_uploaded_by_id_e231fb2a ON console_firmware USING btree (uploaded_by_id);


--
-- Name: console_firmwarelog_node_id_5e7cc699; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX console_firmwarelog_node_id_5e7cc699 ON console_firmwarelog USING btree (node_id);


--
-- Name: console_gateway_client_id_6f26cf99; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX console_gateway_client_id_6f26cf99 ON console_gateway USING btree (client_id);


--
-- Name: console_gateway_connection_id_fb3412c0; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX console_gateway_connection_id_fb3412c0 ON console_gateway USING btree (connection_id);


--
-- Name: console_node_gateway_id_6fdcba1c; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX console_node_gateway_id_6fdcba1c ON console_node USING btree (gateway_id);


--
-- Name: console_nodebattryusage_node_id_5ae1c929; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX console_nodebattryusage_node_id_5ae1c929 ON console_nodebatteryusage USING btree (node_id);


--
-- Name: console_sensor_node_id_271d9082; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX console_sensor_node_id_271d9082 ON console_sensor USING btree (node_id);


--
-- Name: console_sensoractualdata_sensor_id_2e4c88d9; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX console_sensoractualdata_sensor_id_2e4c88d9 ON console_sensoractualdata USING btree (sensor_id);


--
-- Name: console_sensordata_sensor_id_604a19c7; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX console_sensordata_sensor_id_604a19c7 ON console_sensordata USING btree (sensor_id);


--
-- Name: django_admin_log_content_type_id_c4bce8eb; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_admin_log_content_type_id_c4bce8eb ON django_admin_log USING btree (content_type_id);


--
-- Name: django_admin_log_user_id_c564eba6; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_admin_log_user_id_c564eba6 ON django_admin_log USING btree (user_id);


--
-- Name: django_session_expire_date_a5c62663; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_session_expire_date_a5c62663 ON django_session USING btree (expire_date);


--
-- Name: django_session_session_key_c0390e0f_like; Type: INDEX; Schema: public; Owner: postgres
--

CREATE INDEX django_session_session_key_c0390e0f_like ON django_session USING btree (session_key varchar_pattern_ops);


--
-- Name: auth_group_permissions auth_group_permissio_permission_id_84c5c92e_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissio_permission_id_84c5c92e_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_group_permissions auth_group_permissions_group_id_b120cbf9_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_group_permissions
    ADD CONSTRAINT auth_group_permissions_group_id_b120cbf9_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_permission auth_permission_content_type_id_2f476e4b_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_permission
    ADD CONSTRAINT auth_permission_content_type_id_2f476e4b_fk_django_co FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_group_id_97559544_fk_auth_group_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_group_id_97559544_fk_auth_group_id FOREIGN KEY (group_id) REFERENCES auth_group(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_groups auth_user_groups_user_id_6a12ed8b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_groups
    ADD CONSTRAINT auth_user_groups_user_id_6a12ed8b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm FOREIGN KEY (permission_id) REFERENCES auth_permission(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: auth_user_user_permissions auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auth_user_user_permissions
    ADD CONSTRAINT auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: console_client console_client_city_id_207ed370_fk_console_city_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_client
    ADD CONSTRAINT console_client_city_id_207ed370_fk_console_city_id FOREIGN KEY (city_id) REFERENCES console_city(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: console_clientmessagequeue console_clientmessag_client_id_1f1d6530_fk_console_c; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_clientmessagequeue
    ADD CONSTRAINT console_clientmessag_client_id_1f1d6530_fk_console_c FOREIGN KEY (client_id) REFERENCES console_client(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: console_clientmessagequeue console_clientmessag_message_id_7d6deb89_fk_console_c; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_clientmessagequeue
    ADD CONSTRAINT console_clientmessag_message_id_7d6deb89_fk_console_c FOREIGN KEY (message_id) REFERENCES console_clientmessage(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: console_connection console_connection_client_id_id_9301b35b_fk_console_client_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_connection
    ADD CONSTRAINT console_connection_client_id_id_9301b35b_fk_console_client_id FOREIGN KEY (client_id_id) REFERENCES console_client(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: console_firmware console_firmware_node_model_id_0e9774ee_fk_console_n; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_firmware
    ADD CONSTRAINT console_firmware_node_model_id_0e9774ee_fk_console_n FOREIGN KEY (node_model_id) REFERENCES console_nodemodels(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: console_firmware console_firmware_uploaded_by_id_e231fb2a_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_firmware
    ADD CONSTRAINT console_firmware_uploaded_by_id_e231fb2a_fk_auth_user_id FOREIGN KEY (uploaded_by_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: console_firmwarelog console_firmwarelog_node_id_5e7cc699_fk_console_node_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_firmwarelog
    ADD CONSTRAINT console_firmwarelog_node_id_5e7cc699_fk_console_node_id FOREIGN KEY (node_id) REFERENCES console_node(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: console_gateway console_gateway_client_id_6f26cf99_fk_console_client_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_gateway
    ADD CONSTRAINT console_gateway_client_id_6f26cf99_fk_console_client_id FOREIGN KEY (client_id) REFERENCES console_client(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: console_gateway console_gateway_connection_id_fb3412c0_fk_console_connection_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_gateway
    ADD CONSTRAINT console_gateway_connection_id_fb3412c0_fk_console_connection_id FOREIGN KEY (connection_id) REFERENCES console_connection(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: console_node console_node_gateway_id_6fdcba1c_fk_console_gateway_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_node
    ADD CONSTRAINT console_node_gateway_id_6fdcba1c_fk_console_gateway_id FOREIGN KEY (gateway_id) REFERENCES console_gateway(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: console_nodebatteryusage console_nodebattryusage_node_id_5ae1c929_fk_console_node_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_nodebatteryusage
    ADD CONSTRAINT console_nodebattryusage_node_id_5ae1c929_fk_console_node_id FOREIGN KEY (node_id) REFERENCES console_node(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: console_sensor console_sensor_node_id_271d9082_fk_console_node_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_sensor
    ADD CONSTRAINT console_sensor_node_id_271d9082_fk_console_node_id FOREIGN KEY (node_id) REFERENCES console_node(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: console_sensoractualdata console_sensoractual_sensor_id_2e4c88d9_fk_console_s; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_sensoractualdata
    ADD CONSTRAINT console_sensoractual_sensor_id_2e4c88d9_fk_console_s FOREIGN KEY (sensor_id) REFERENCES console_sensor(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: console_sensordata console_sensordata_sensor_id_604a19c7_fk_console_sensor_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY console_sensordata
    ADD CONSTRAINT console_sensordata_sensor_id_604a19c7_fk_console_sensor_id FOREIGN KEY (sensor_id) REFERENCES console_sensor(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_content_type_id_c4bce8eb_fk_django_co; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_content_type_id_c4bce8eb_fk_django_co FOREIGN KEY (content_type_id) REFERENCES django_content_type(id) DEFERRABLE INITIALLY DEFERRED;


--
-- Name: django_admin_log django_admin_log_user_id_c564eba6_fk_auth_user_id; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY django_admin_log
    ADD CONSTRAINT django_admin_log_user_id_c564eba6_fk_auth_user_id FOREIGN KEY (user_id) REFERENCES auth_user(id) DEFERRABLE INITIALLY DEFERRED;


--
-- PostgreSQL database dump complete
--

\connect postgres

SET default_transaction_read_only = off;

--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: postgres; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON DATABASE postgres IS 'default administrative connection database';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- PostgreSQL database dump complete
--

\connect template1

SET default_transaction_read_only = off;

--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.1

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: template1; Type: COMMENT; Schema: -; Owner: postgres
--

COMMENT ON DATABASE template1 IS 'default template for new databases';


--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


--
-- PostgreSQL database dump complete
--

--
-- PostgreSQL database cluster dump complete
--

