cd /usr/local/fox/
su - postgres
rm ./backups/all_databases.sql
pg_dumpall > ./backups/all_databases.sql
grep "^[\]connect" ./backups/all_databases.sql