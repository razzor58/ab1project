cd /usr/local/fox/

# change user
su - postgres

# remove old backups
rm ./backups/fox.bak
rm ./backups/postgres.bak
rm ./backups/template1.bak

# create new
pg_dump -Fc fox > ./backups/fox.bak
pg_dump -Fc postgres > ./backups/postgres.bak
pg_dump -Fc template1 > ./backups/template1.bak
