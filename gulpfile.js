var gulp = require('gulp'),
    gp_concat = require('gulp-concat'),
    gp_rename = require('gulp-rename'),
    gp_uglify = require('gulp-uglify'),
    gp_mincss = require('gulp-clean-css');

var data = require('gulp-data');

gulp.task('tms-vendor-js', function(){
    return gulp.src(
            ['bower_components/jquery/dist/jquery.min.js',
            'bower_components/bootstrap/dist/js/bootstrap.min.js',
            'bower_components/datatables.net/js/jquery.dataTables.min.js',
            'bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js',
            'bower_components/datatables.net-fixedcolumns/js/dataTables.fixedColumns.min.js',
            'bower_components/datatables.net-buttons/js/dataTables.buttons.min.js',
            'bower_components/datatables.net-buttons-bs/js/buttons.bootstrap.min.js',
            'bower_components/datatables.net-buttons/js/buttons.print.min.js',
            'bower_components/datatables.net-buttons/js/buttons.html5.min.js',
            'bower_components/datatables.net-buttons/js/buttons.colVis.min.js',
            'bower_components/jszip/dist/jszip.min.js',
            'bower_components/moment/min/moment-with-locales.min.js',
            'bower_components/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js',
            'bower_components/bootstrap-daterangepicker/daterangepicker.js',
            'bower_components/suggestions.jquery/dist/js/jquery.suggestions.min.js',
			'bower_components/select2/dist/js/select2.min.js',
			'bower_components/select2/dist/js/i18n/ru.js',
			'bower_components/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js']
        )
        .pipe(gp_concat('vendor.min.js'))
        .pipe(gulp.dest('static/tms/js'));
});

gulp.task('tms-vendor-css', function(){
    return gulp.src(
            ['bower_components/bootstrap/dist/css/bootstrap.min.css',
            'bower_components/bootswatch/yeti/bootstrap.min.css',
            'bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css',
            'bower_components/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css',
            'bower_components/bootstrap-daterangepicker/daterangepicker.css',
            'bower_components/font-awesome/css/font-awesome.min.css',
            'bower_components/suggestions.jquery/dist/css/suggestions.css',
			'bower_components/select2/dist/css/select2.css',
			'bower_components/select2-bootstrap-theme/dist/select2-bootstrap.min.css',
			'bower_components/datatables.net-fixedheader/css/fixedHeader.dataTables.min.css',
			'bower_components/datatables.net-fixedheader/css/fixedHeader.jqueryui.min.css',
            'bower_components/datatables.net-buttons-bs/css/buttons.bootstrap.min.css']
        )
        .pipe(gp_concat('vendor.min.css'))
        .pipe(gulp.dest('static/tms/css'));
});

gulp.task('tms-vendor-images', function(){
    return gulp.src(
            ['bower_components/datatables.net-dt/images/*']
        )
        .pipe(gulp.dest('static/tms/images'));
});

gulp.task('tms-js', function(){
    return gulp.src(['TrackManager/static/js/*.js'])
//        .pipe(gp_uglify())
        .pipe(gp_rename({suffix: '.min'}))
        .pipe(gulp.dest('static/tms/js'));
});

gulp.task('tms-css', function(){
    return gulp.src(['TrackManager/static/css/*.css'])
        .pipe(gp_mincss())
        .pipe(gp_rename({suffix: '.min'}))
        .pipe(gulp.dest('static/tms/css'));
});

gulp.task('tms-copy-fonts', function () {
    return gulp.src([
      'bower_components/bootstrap/dist/fonts/**/*',
      'bower_components/font-awesome/fonts/*'
    ]).pipe(gulp.dest('static/tms/fonts'));
});

// run them like any other task
gulp.task('default', [
  'tms-vendor-js',
  'tms-vendor-css',
  'tms-vendor-images',
  'tms-js',
  'tms-css',
  'tms-copy-fonts'
]);
